<?php
/**
 * Modal search
 */
?>

<div class="eco-modal" id="modal-search">
    <div class="eco-modal__wrap">
        <div class="eco-modal__content">
            <button class="eco-btn eco-modal__close">
                <svg width="18" height="17" viewBox="0 0 18 17" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <line x1="0.781212" y1="16.2028" x2="16.3376" y2="0.646494" stroke="#0C2A50" />
                    <line x1="1.34733" y1="0.777049" x2="17.1728" y2="16.0595" stroke="#0C2A50" />
                </svg>
            </button>

            <h2 class="eco-modal__title mb-30"><?php esc_html_e('What are you looking for?', ECO_PREFIX); ?></h2>

            <div class="row">
                <div class="col-xl-11">
                    <form  role="search" method="get" class="search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
                        <div class="d-flex eco-filter__row">
                            <div class="eco-field-form field_search">
                                <input type="text" name="s" value="<?php echo get_search_query(); ?>" placeholder="<?php esc_attr_e('Search by keyword', ECO_PREFIX); ?>">
                            </div>

                            <button class="eco-btn eco-btn_icon eco-btn_main">
                                <?php esc_html_e('Search', ECO_PREFIX); ?>
                                <span class="icon"></span>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>