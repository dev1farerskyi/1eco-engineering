<div class="eco-section-element <?php echo $args['classes'] ?>" id="<?php echo esc_attr( $args['id'] ); ?>">
    <div class="container">
        <div class="eco-invite__wrap">
            <div class="d-flex flex-column flex-lg-row eco-invite__content">
                <div class="eco-invite__text">
                    <?php if ( ! empty( $args['text'] ) ) : ?>
                        <h2 class="eco-block-title color-white"><?php echo $args['text']; ?></h2>
                    <?php endif ?>

                    <?php if ( ! empty( $args['description'] ) ) : ?>
                        <p class="eco-block-subtitle color-white"><?php echo $args['description']; ?></p>
                    <?php endif ?>
                </div>
                <?php eco_btn($args['button'], 'eco-btn eco-btn_icon eco-btn_main ml-lg-40 ml-0'); ?>
            </div>
        </div>
    </div>
</div>
