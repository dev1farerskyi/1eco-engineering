<?php
/**
 * Find industry
 */

if ( ! is_front_page() ) {
    return;
}
?>

<div class="eco-find-industry eco-find-cont">
    <div class="eco-find-industry__wrap js-open-industry">
        <div class="eco-find-industry__text">
            <?php esc_html_e('Find Industry', ECO_PREFIX); ?>
        </div>

        <button class="eco-find-industry__button">
            <img src="<?php echo ECO_TEMP_URL . '/assets/img/find_1.svg'; ?>" alt="icon">
        </button>
    </div>

    <div class="eco-find-industry__content">
        <?php wp_nav_menu(
            array(
                'container' => '',
                'items_wrap' => '<ul>%3$s</ul>',
                'theme_location' => 'find-industry-menu',
                'depth' => 1,
                'fallback_cb' => '__return_empty_string',
            )
        ); ?>

        <div class="eco-find-industry__wrap-bottom js-close-industry">
            <div class="eco-find-industry__text">
                <?php esc_html_e('Find Industry', ECO_PREFIX); ?>
            </div>

            <button class="eco-find-industry__button">
                <img src="<?php echo ECO_TEMP_URL . '/assets/img/find_close.svg'; ?>" alt="icon">
            </button>
        </div>
    </div>
</div>
