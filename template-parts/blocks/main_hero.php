<?php
/*
 * Block Name: Main Hero Block
 * Slug:
 * Description:
 * Keywords:
 * Dependency:
 * Align: false
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

$subtitle = get_field('subtitle');
$title = get_field('title');
$button_1 = get_field('button_1');
$button_2 = get_field('button_2');
$image = get_field('image');
$numbers_counter = get_field('numbers_counter');

$block_name = 'eco-main_hero';
// Create id attribute allowing for custom "anchor" value.
$id = $block_name . '-' . $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}
// Create class attribute allowing for custom "className" and "align" values.
$className   = array( $block_name );
$className[] = 'eco-section-element';
?>
<div class="<?php echo implode( ' ', $className ); ?>" id="<?php echo esc_attr( $id ); ?>">
    <div class="eco-main-banner__bg">
        <div class="eco-main-banner__bg-image">
            <?php if ( ! empty( $image ) ) : ?>
                <img src="<?php echo esc_url($image['url']); ?>" alt="image">
            <?php endif ?>
        </div>

        <div class="eco-main-banner__bg-circle"></div>
    </div>

    <div class="eco-main-banner__content">
        <div class="container">
            <div class="row">
                <div class="col-xl-5 col-lg-6 col-md-9 col-sm-10">
                    <?php if ( ! empty( $subtitle ) ) : ?>
                        <div class="eco-block-subtitle"><?php echo $subtitle; ?></div>
                    <?php endif ?>

                    <?php if ( ! empty( $title ) ) : ?>
                        <h1 class="eco-block-title mb-sm-35 mb-30"><?php echo $title; ?></h1>
                    <?php endif ?>

                    <div class="d-flex flex-column flex-sm-row">
                        <?php eco_btn($button_1, 'eco-btn eco-btn_icon eco-btn_main mr-sm-30 mb-sm-0 mb-25'); ?>
                        <?php eco_btn($button_2, 'eco-btn eco-btn_icon eco-btn_border'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php if ( ! empty( $numbers_counter ) ) : ?>
        <div class="eco-statistics pos_banner">
            <div class="container">
                <div class="eco-statistics__wrap">
                    <div class="eco-statistics__list">
                        <?php foreach( $numbers_counter as $row ): ?>
                            <div class="eco-statistics__col">
                                <div class="eco-statistics__col-number-count">
                                    <h2 class="eco-statistics__col-number">
                                        <?php echo $row['number']; ?>
                                    </h2>
                                    <h2 class="eco-statistics__col-number-after"><?php echo $row['after_number']; ?></h2>
                                </div>

                                <div class="eco-statistics__col-text"><?php echo $row['description']; ?></div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
    <?php endif; ?>
</div>
