<?php
/*
// * Block Name: Text Section Post Block
// * Slug:
// * Description:
// * Keywords:
// * Dependency:
// * Align: false
// *
// * @param   array $block The block settings and attributes.
// * @param   string $content The block inner HTML (empty).
// * @param   bool $is_preview True during AJAX preview.
// * @param   (int|string) $post_id The post ID this block is saved to.
 */
//
$content = get_field('content');

$block_name = 'eco-text_section_content';
// Create id attribute allowing for custom "anchor" value.
$id = $block_name . '-' . $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}
// Create class attribute allowing for custom "className" and "align" values.
$className   = array( $block_name );
$className[] = 'eco-section-element';
?>
<div class="<?php echo implode( ' ', $className ); ?>" id="<?php echo esc_attr( $id ); ?>">
    <div class="container-small">
        <?php if ( ! empty( $content ) ) : ?>
            <div class="eco-text_section_content__wrap">
                <?php echo $content; ?>
            </div>
        <?php endif ?>
    </div>
</div>










