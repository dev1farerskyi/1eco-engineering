<?php
/*
 * Block Name: Map Block
 * Slug:
 * Description:
 * Keywords:
 * Dependency:
 * Align: false
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

$subtitle = get_field('subtitle');
$title = get_field('title');
$countries = get_field('countries');

$block_name = 'eco-map';
// Create id attribute allowing for custom "anchor" value.
$id = $block_name . '-' . $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}
// Create class attribute allowing for custom "className" and "align" values.
$className   = array( $block_name );
$className[] = 'eco-section-element';
?>
<div class="<?php echo implode( ' ', $className ); ?>" id="<?php echo esc_attr( $id ); ?>">
    <div class="container">
        <div class="eco-map__head">
            <?php if ( ! empty( $title ) ) : ?>
                <h2 class="eco-block-title"><?php echo $title; ?></h2>
            <?php endif ?>
            <?php if ( ! empty( $subtitle ) ) : ?>
                <p class="eco-block-subtitle mb-lg-60 mb-30"><?php echo $subtitle; ?></p>
            <?php endif ?>
        </div>

        <?php if ( ! empty( $countries ) ) : ?>
            <div class="eco-tabs d-none d-md-block">
                <div class="row justify-content-center">
                    <div class="col-xl-9">
                        <div class="eco-tabs__list mb-80">
                            <?php foreach ( $countries as $key => $item ) : ?>
                                <div class="eco-tabs__btn <?php echo $key === 0 ? 'active' : ''; ?>" data-tab-content="tab-content-<?php echo $key; ?>"><?php echo $item['country']; ?></div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
            </div>

            <div class="d-block d-md-none">
                <div class="eco-info-tabs__filter">
                    <div class="eco-field-form field_select">
                        <select class="select2 js-change-map" style="width: 100%">
                            <?php foreach ( $countries as $key => $item ) : ?>
                                <option value="tab-content-<?php echo $key; ?>"><?php echo $item['country']; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>
            </div>

            <div class="tab-content">
                <?php foreach ( $countries as $key => $item ) : ?>
                    <div class="tab-content-item <?php echo $key === 0 ? 'active' : ''; ?>" id="tab-content-<?php echo $key; ?>">
                        <?php echo do_shortcode( '[mapplic id="' . $item['shortcode_id'] . '"]' ); ?>
                    </div>
                <?php endforeach; ?>
            </div>
        <?php endif; ?>
    </div>
</div>















