<?php
/*
 * Block Name: Two blocks section Block
 * Slug:
 * Description:
 * Keywords:
 * Dependency:
 * Align: false
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

$posts = get_field('posts');

$args = array(
    'post_type' => array('projects', 'post', 'industry'),
    'post_status' => 'publish',
    'post_per_page' => -1,
    'post__in' => $posts,
);
$posts_query = new WP_Query($args);

$block_name = 'eco-two_blocks_section';
// Create id attribute allowing for custom "anchor" value.
$id = $block_name . '-' . $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}
// Create class attribute allowing for custom "className" and "align" values.
$className   = array( $block_name );
$className[] = 'eco-section-element';

if ( $posts_query->have_posts() ) : ?>
    <div class="<?php echo implode( ' ', $className ); ?>" id="<?php echo esc_attr( $id ); ?>">
        <div class="container">
            <div class="eco-simple-slider__wrap">
                <div class="swiper swiper-simple-slider">
                    <div class="swiper-wrapper">
                        <?php while ( $posts_query->have_posts() ) : $posts_query->the_post();
                            $post_id = get_the_ID();
                            $title = get_field( 'title', $post_id );
                            $blurb_description = get_field( 'blurb_description', $post_id );
                            $button = get_field( 'button', $post_id );
                            ?>
                            <div class="swiper-slide">
                                <div class="eco-two_blocks news_min">
                                    <div class="eco-two_blocks__wrap">
                                        <div class="eco-two_blocks__content">
                                            <?php if ( get_post_type($post_id) !== 'industry' ) :
                                                $services = get_the_terms($post_id, get_post_type($post_id) . '_services');
                                                $industries = get_the_terms($post_id, get_post_type($post_id) . '_industry');
                                                ?>
                                                <ul class="eco-two_blocks__tags">
                                                    <?php if ( ! empty( $services ) ) : ?>
                                                        <li>
                                                            <?php foreach ( $services as $service ) : ?>
                                                                <a href="<?php echo get_term_link($service, 'post_services'); ?>"><?php echo $service->name; ?></a>
                                                            <?php endforeach; ?>
                                                        </li>
                                                    <?php endif; ?>

                                                    <?php if ( ! empty( $industries ) ) : ?>
                                                        <li>
                                                            <?php foreach ( $industries as $key => $industry ) : ?>
                                                                <a href="<?php echo get_term_link($industry, 'post_industry'); ?>"><?php echo $industry->name; ?></a>
                                                            <?php endforeach; ?>
                                                        </li>
                                                    <?php endif; ?>
                                                </ul>
                                            <?php endif; ?>

                                            <?php the_title('<h5 class="eco-two_blocks__title">', '</h5>'); ?>

                                            <div class="eco-two_blocks__text">
                                                <?php the_excerpt(); ?>
                                            </div>

                                            <div class="eco-two_blocks__btn">
                                                <?php eco_btn(array('url' => get_the_permalink(), 'title' => esc_html__('Learn More', ECO_PREFIX)), 'eco-btn eco-btn_icon'); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endwhile; wp_reset_postdata(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif;