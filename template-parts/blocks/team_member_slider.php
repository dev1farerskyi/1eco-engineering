<?php
/*
 * Block Name: Team Member Slider Block
 * Slug:
 * Description:
 * Keywords:
 * Dependency:
 * Align: false
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

$title = get_field('title');
$members = get_field('members');

$block_name = 'eco-team';
// Create id attribute allowing for custom "anchor" value.
$id = $block_name . '-' . $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}
// Create class attribute allowing for custom "className" and "align" values.
$className   = array( $block_name );
$className[] = 'eco-section-element';

$args = array(
    'post_type' => 'members',
    'post_status' => 'publish',
    'post_per_page' => -1,
    'post__in' => $members,
);
$members_query = new WP_Query($args);
?>

<div class="<?php echo implode( ' ', $className ); ?>" id="<?php echo esc_attr( $id ); ?>">
    <div class="container">
        <div class="eco-recent__head">
            <?php if ( ! empty( $title ) ) : ?>
                <h2 class="eco-block-title"><?php echo $title; ?></h2>
            <?php endif ?>

            <div class="swiper-cont">
                <div class="swiper-button-prev swiper-team-prev">
                    <span class="icon">
                        <svg width="7" height="12" viewBox="0 0 7 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M6 1L1 6L6 11" stroke="#0C2A50" stroke-linecap="round"></path>
                        </svg>
                    </span>
                </div>
                <div class="swiper-button-next swiper-team-next">
                    <span class="icon">
                        <svg width="7" height="12" viewBox="0 0 7 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M1 11L6 6L1 1" stroke="#0C2A50" stroke-linecap="round"></path>
                        </svg>
                    </span>
                </div>
            </div>
        </div>

        <?php if ( $members_query->have_posts() ) : ?>
            <div class="eco-team__wrap">
                <div class="swiper swiper-team">
                    <div class="swiper-wrapper">
                        <?php while ($members_query->have_posts()) : $members_query->the_post();
                            $job_title = get_field( 'job_title', get_the_ID() );
                            $main_image = get_field( 'main_image', get_the_ID() );
                            $linkedin_link = get_field( 'linkedin_link', get_the_ID() );
                            ?>
                            <div class="swiper-slide">
                                <div class="eco-team__item">
                                    <div class="eco-team__item-rect"></div>
                                    <div class="eco-team__item-wrap">
                                        <div class="eco-team__item-image">
                                            <img src="<?php echo esc_url($main_image['url']); ?>" alt="image">
                                        </div>

                                        <div class="eco-team__item-info">
                                            <div class="eco-team__item-bg"></div>
                                            <div class="eco-team__item-position"><?php echo esc_html( $job_title ); ?></div>

                                            <?php the_title('<h5 class="eco-team__item-name">', '</h5>'); ?>

                                            <?php eco_btn($linkedin_link, 'eco-btn_icon-social eco-btn eco-btn_icon'); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endwhile; wp_reset_postdata();
                        ?>
                    </div>
                </div>
            </div>
        <?php endif; ?>
    </div>
</div>
