<?php
/*
 * Block Name: Section with fixed scroll Block
 * Slug:
 * Description:
 * Keywords:
 * Dependency:
 * Align: false
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

$description = get_field('description');
$title = get_field('title');
$button = get_field('button');
$tabs = get_field('tabs');
$layout = get_field('layout');

$block_name = 'eco-section_with_fixed_scroll';
// Create id attribute allowing for custom "anchor" value.
$id = $block_name . '-' . $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}
// Create class attribute allowing for custom "className" and "align" values.
$className   = array( $block_name );
$className[] = 'eco-section-element';
?>
<div class="<?php echo implode( ' ', $className ); ?>" id="<?php echo esc_attr( $id ); ?>">
    <div class="container <?php echo $layout; ?>">
        <div class="row justify-content-center">
            <div class="col-lg-12">
                <div class="eco-block-center mb-60 mb-md-0">
                    <div class="row">
                        <div class="<?php echo $layout == 'two-columns' ? 'col-lg-6' : 'col-lg-12'; ?>">
                            <?php if ( ! empty( $title ) ) : ?>
                                <h2 class="eco-block-title"><?php echo $title; ?></h2>
                            <?php endif ?>
                        </div>

                        <div class="<?php echo $layout == 'two-columns' ? 'col-lg-6' : 'col-lg-12'; ?>">
                            <?php if ( ! empty( $description ) ) : ?>
                                <div class="eco-block-subtitle mb-80"><?php echo $description; ?></div>
                            <?php endif ?>

                            <?php eco_btn($button, 'eco-btn eco-btn_icon eco-btn_border mt-sm-0 mt-n10 mx-sm-auto mb-lg-50 mb-50'); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php if ( ! empty( $tabs ) ) : ?>
            <div class="row align-items-stretch">
                <div class="col-lg-6">
                    <div class="d-block d-lg-none">
                        <div class="eco-info-tabs__filter">
                            <div class="eco-field-form field_select field_select--dark">
                                <select class="select2 js-change-info-tabs" style="width: 100%">
                                    <?php foreach ($tabs as $index => $item):?>
                                        <option value="tab-<?php echo $index; ?>"><?php echo $item['tab_name']; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="eco-info-tabs__list d-none d-lg-block">
                        <?php foreach( $tabs as $index => $item ): ?>
                            <?php $tab_name = $item['tab_name']; ?>
                            <?php if($tab_name): ?>
                                <a href="javascript:void(0)" data-tab="<?php echo 'tab-' . $index; ?>" class="eco-info-tabs__link <?php echo $index === 0 ? 'active' : ''; ?>">
                                    <span class="icon">
                                        <span class="icon-arrow">
                                            <svg width="25" height="8" viewBox="0 0 25 8" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M24.3536 4.35355C24.5488 4.15829 24.5488 3.84171 24.3536 3.64645L21.1716 0.464466C20.9763 0.269204 20.6597 0.269204 20.4645 0.464466C20.2692 0.659728 20.2692 0.976311 20.4645 1.17157L23.2929 4L20.4645 6.82843C20.2692 7.02369 20.2692 7.34027 20.4645 7.53553C20.6597 7.7308 20.9763 7.7308 21.1716 7.53553L24.3536 4.35355ZM24 3.5L0 3.5V4.5L24 4.5V3.5Z" fill="#B7E588"></path>
                                            </svg>
                                        </span>
                                    </span>

                                    <span class="eco-info-tabs__link-text">
                                        <?php echo $tab_name; ?>
                                    </span>
                                </a>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </div>
                </div>

                <div class="col-lg-6">
                    <div class="eco-info-tabs__content">
                        <?php foreach( $tabs as $index => $item ): ?>
                            <?php $icon = $item['icon']; ?>
                            <?php $title = $item['title']; ?>
                            <?php $description = $item['description']; ?>
                            <?php $description_excerpt = $item['description_excerpt']; ?>
                            <div class="eco-info-tabs__box <?php echo $index === 0 ? 'active' : '';  ?>" id="<?php echo 'tab-' . $index; ?>">
                                <div class="eco-info-tabs__image">
                                    <div class="eco-info-tabs__image-wrap">
                                        <img src="<?php echo esc_url( $item['image']['url'] ); ?>" alt="image">
                                    </div>
                                    <?php if( ! empty( $description ) ): ?>
                                    <div class="eco-info-tabs__image-icon">
                                        <img src="<?php echo esc_url( $icon['url'] ); ?>" alt="icon">
                                    </div>
                                    <?php endif; ?>
                                </div>

                                <?php if ( ! empty( $title ) ): ?>
                                    <h3 class="eco-info-tabs__box-title"><?php echo $title; ?></h3>
                                <?php endif; ?>

                                <?php if ( ! empty( $description ) ): ?>
                                    <div class="eco-info-tabs__box-text d-none d-lg-block"><?php echo $description; ?></div>
                                <?php endif; ?>

                                <?php if ( ! empty( $description_excerpt ) ): ?>
                                    <div class="eco-info-tabs__box-text d-block d-lg-none"><?php echo $description_excerpt; ?></div>
                                <?php endif; ?>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        <?php endif ?>
    </div>
</div>
