<?php
/*
 * Block Name: Large Image/Video Block
 * Slug:
 * Description:
 * Keywords:
 * Dependency:
 * Align: false
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

$slider_project = get_field('slider_project');
$image = get_field('image');

$block_name = 'eco-slider-project-page';
// Create id attribute allowing for custom "anchor" value.
$id = $block_name . '-' . $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}
// Create class attribute allowing for custom "className" and "align" values.
$className   = array( $block_name );
$className[] = 'eco-section-element';
$className[] = 'eco-image-video-block';
?>

<div class="<?php echo implode( ' ', $className ); ?>" id="<?php echo esc_attr( $id ); ?>">
    <div class="container">
        <div class="container-small">
            <?php if ( ! empty( $slider_project ) ) : ?>
                <div class="swiper-button-cont-project">
                    <div class="swiper-custom-prev">
                        <img class="" src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/img/arrow-prev.svg" alt="address">
                    </div>
                    <div class="swiper-custom-next">
                        <img class="" src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/img/arrow-next.svg" alt="address">
                    </div>
                </div>
                <div class="eco-slider-project-page__wrap">
                    <div class="swiper gallery-top">
                        <div class="swiper-wrapper">
                            <?php foreach ($slider_project as $slide):?>
                                <div class="swiper-slide" style="background-image:url('<?php echo $slide ['slide']['url']; ?>')"></div>
                            <?php endforeach; ?>
                        </div>
                        <div class="swiper-button-next swiper-button-white"></div>
                        <div class="swiper-button-prev swiper-button-white"></div>
                    </div>
                    <div class="swiper gallery-thumbs">
                        <div class="swiper-wrapper">
                            <?php foreach ($slider_project as $slide):?>
                                <div class="swiper-slide" style="background-image:url('<?php echo $slide ['slide']['url']; ?>')"></div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
            <?php endif; ?>

            <?php if ( ! empty( $image ) ) : ?>
                <div class="image-block">
                    <img class="eco-image-video-block__img" src="<?php echo $image['url']; ?>" alt="image">
                </div>
            <?php endif ?>
        </div>
    </div>
</div>
