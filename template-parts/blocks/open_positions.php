<?php
/*
 * Block Name: Open Positions Block
 * Slug:
 * Description:
 * Keywords:
 * Dependency:
 * Align: false
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

$title = get_field('title');
$positions = get_field('positions');
$job_type = get_field('job_type');

$block_name = 'eco-open_positions';
// Create id attribute allowing for custom "anchor" value.
$id = $block_name . '-' . $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}
// Create class attribute allowing for custom "className" and "align" values.
$className   = array( $block_name );
$className[] = 'eco-section-element';

// Get terms
$locations = get_terms(array(
    'taxonomy' => 'location',
    'hide_empty' => false,
));

$departments = get_terms(array(
    'taxonomy' => 'department',
    'hide_empty' => false,
));

// Positions query
$args = array(
    'post_type' => 'open_positions',
    'post_status' => 'publish',
    'posts_per_page' => -1,
);

if ( ! empty( $positions ) ) {
    $args['post__in'] = $positions;
}

$positions_query = new WP_Query( $args );
?>

<div class="<?php echo implode( ' ', $className ); ?>" id="<?php echo esc_attr( $id ); ?>">
    <div class="container">
        <?php if ( ! empty( $title ) ) : ?>
            <h2 class="eco-block-title mb-lg-50 mb-sm-30 mb-10"><?php echo $title; ?></h2>
        <?php endif ?>

        <div class="eco-vacancies__filter">
            <div class="eco-filter">
                <h4 class="eco-filter__title"><?php esc_html_e('Filter by', ECO_PREFIX); ?></h4>
                <form action="#" class="js-sorting-positions">
                    <div class="row">
                        <?php if ( ! empty( $locations ) ) : ?>
                            <div class="col-lg-3 col-sm-6">
                                <div class="eco-field-form field_select">
                                    <select class="select2" name="location" data-placeholder="Location" style="width: 100%">
                                        <option value="all"><?php esc_html_e('All Locations', ECO_PREFIX); ?></option>

                                        <?php foreach ( $locations as $location ) : ?>
                                            <option value="<?php echo $location->term_id; ?>"><?php echo $location->name; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                        <?php endif; ?>

                        <?php if ( ! empty( $departments ) ) : ?>
                            <div class="col-lg-3 col-sm-6">
                                <div class="eco-field-form field_select">
                                    <select class="select2" name="department" data-placeholder="Department" style="width: 100%">
                                        <option value="all"><?php esc_html_e('All Departments', ECO_PREFIX); ?></option>

                                        <?php foreach ( $departments as $department ) : ?>
                                            <option value="<?php echo $department->term_id; ?>"><?php echo $department->name; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                        <?php endif; ?>

                        <div class="col-lg-6">
                            <div class="d-flex eco-filter__row">
                                <div class="eco-field-form field_search">
                                    <input type="text" name="s" placeholder="<?php esc_attr_e('Search by keyword', ECO_PREFIX); ?>">
                                </div>

                                <button type="submit" class="eco-btn eco-btn_icon eco-btn_main w-100">
                                    <?php esc_html_e('Search', ECO_PREFIX); ?>
                                    <span class="icon"></span>
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <div class="js-posts-wrapper">
            <?php if ( $positions_query->have_posts() ) : ?>
                <?php while ( $positions_query->have_posts() ) : $positions_query->the_post();
                    get_template_part('template-parts/position-content');
                endwhile; wp_reset_postdata();
            endif; ?>
        </div>
    </div>
</div>











