<?php
/*
 * Block Name: Small Hero Block
 * Slug:
 * Description:
 * Keywords:
 * Dependency:
 * Align: false
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

$subtitle = get_field('subtitle');
$title = get_field('title');
$text = get_field('text');
$button = get_field('button');
$image = get_field('image');
$numbers  = get_field('numbers');
$rounding = get_field('rounding') ? 'rounding' : '';

$block_name = 'eco-hero';
// Create id attribute allowing for custom "anchor" value.
$id = $block_name . '-' . $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}
// Create class attribute allowing for custom "className" and "align" values.
$className   = array( $block_name );
$className[] = 'eco-section-element';

if ( ! empty( $numbers ) ) {
    $className[] = 'hero_long';
}

if ( ! empty( $image ) ) {
    $className[] = 'eco-hero--with-image';
}

?>
<div class="<?php echo implode( ' ', $className ); ?>" id="<?php echo esc_attr( $id ); ?>">
    <div class="eco-hero__wrap">
        <div class="container">
            <div class="eco-hero__content">
                <div class="row">
                    <div class="col-lg-6">
                        <?php if ( ! empty( $subtitle ) ) : ?>
                            <h6 class="eco-hero__subtitle"><?php echo $subtitle; ?></h6>
                        <?php endif; ?>

                        <?php if ( ! empty( $title ) ) : ?>
                            <h1 class="eco-hero__title"><?php echo $title; ?></h1>
                        <?php endif; ?>

                        <?php if ( ! empty( $image ) ) : ?>
                            <?php if ( ! empty( $text ) ) : ?>
                                <div class="eco-hero__text">
                                    <p><?php echo $text; ?></p>
                                </div>
                            <?php endif ?>

                            <?php if ( ! empty( $button ) ) : ?>
                                <div class="d-flex flex-column flex-sm-row">
                                    <?php eco_btn($button, 'eco-btn eco-btn_icon eco-btn_main mr-sm-30 mb-sm-0 mb-25'); ?>
                                </div>
                            <?php endif; ?>
                        <?php endif; ?>
                    </div>

                    <?php if ( empty( $image ) ) : ?>
                        <div class="col-lg-6">
                            <?php if ( ! empty( $text ) ) : ?>
                                <div class="eco-hero__text">
                                    <p><?php echo $text; ?></p>
                                </div>
                            <?php endif ?>

                            <?php if ( ! empty( $button ) ) : ?>
                                <div class="d-flex flex-column flex-sm-row">
                                    <?php eco_btn($button, 'eco-btn eco-btn_icon eco-btn_main mr-sm-30 mb-sm-0 mb-25'); ?>
                                </div>
                            <?php endif; ?>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>

    <?php if ( ! empty( $image ) ) : ?>
        <img class="eco-hero__img" src="<?php echo esc_url( $image['url'] ); ?>" alt="image">
    <?php endif; ?>
</div>

<?php if ( ! empty( $numbers ) ) : ?>
    <div class="eco-statistics line_white position_left pos_top eco-section-element">
        <div class="container">
            <div class="eco-statistics__wrap">
                <div class="eco-statistics__list">
                    <?php foreach( $numbers as $row ): ?>
                        <?php $number = $row['number']; ?>
                        <?php $after_number = $row['after_number']; ?>
                        <?php $description = $row['description']; ?>
                        <?php if($description): ?>
                            <div class="eco-statistics__col">
                                <div class="eco-statistics__col-number-count">
                                    <?php if($number): ?>
                                        <h2 class="eco-statistics__col-number">
                                            <?php echo $number; ?>
                                        </h2>
                                    <?php endif; ?>
                                    <?php if($after_number): ?>
                                        <h2 class="eco-statistics__col-number-after"><?php echo $after_number; ?></h2>
                                    <?php endif; ?>
                                </div>
                                <p class="eco-statistics__col-text"><?php echo $description; ?></p>
                            </div>
                        <?php endif; ?>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>
<?php endif;
