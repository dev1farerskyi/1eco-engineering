<?php
/*
// * Block Name: Three Card Slider Block
// * Slug:
// * Description:
// * Keywords:
// * Dependency:
// * Align: false
// *
// * @param   array $block The block settings and attributes.
// * @param   string $content The block inner HTML (empty).
// * @param   bool $is_preview True during AJAX preview.
// * @param   (int|string) $post_id The post ID this block is saved to.
 */
//
$title = get_field('title');
$button = get_field('button');
$posts = get_field('block');

$args = array(
    'post_type' => array('industry', 'post'),
    'post_status' => 'publish',
    'posts_per_page' => -1,
);

if ( !empty($posts) ) {
    $args['post__in'] = $posts;
}

$query_posts = new WP_Query($args);

$block_name = 'eco-three_card_slider';
// Create id attribute allowing for custom "anchor" value.
$id = $block_name . '-' . $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}
// Create class attribute allowing for custom "className" and "align" values.
$className   = array( $block_name );
$className[] = 'eco-section-element';
?>

<div class="<?php echo implode( ' ', $className ); ?>" id="<?php echo esc_attr( $id ); ?>">
    <div class="container">
        <?php if ( ! empty( $title ) ) : ?>
            <div class="eco-recent__head">
                <h2 class="eco-block-title"><?php echo $title; ?></h2>

                <?php if ( $query_posts->post_count >= 4 ) : ?>
                    <div class="swiper-cont swiper-cont--desktop">
                        <div class="swiper-button-prev swiper-recent-news-prev">
                            <span class="icon">
                                <svg width="7" height="12" viewBox="0 0 7 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M6 1L1 6L6 11" stroke="#0C2A50" stroke-linecap="round"></path>
                                </svg>
                            </span>
                        </div>

                        <div class="swiper-button-next swiper-recent-news-next">
                            <span class="icon">
                                <svg width="7" height="12" viewBox="0 0 7 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M1 11L6 6L1 1" stroke="#0C2A50" stroke-linecap="round"></path>
                                </svg>
                            </span>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        <?php endif; ?>

        <div class="eco-recent__wrap">
            <div class="swiper swiper-recent-news">
                <div class="swiper-wrapper">
                    <?php while ($query_posts->have_posts()) : $query_posts->the_post(); ?>
                        <div class="swiper-slide">
                            <?php get_template_part('template-parts/content', null, array('class' => 'news_short')); ?>
                        </div>
                    <?php endwhile; wp_reset_postdata();?>
                </div>
            </div>

            <?php if ( $query_posts->post_count >= 4 ) : ?>
                <div class="swiper-cont swiper-cont--mobile">
                    <div class="swiper-button-prev swiper-recent-news-prev">
                        <span class="icon">
                            <svg width="7" height="12" viewBox="0 0 7 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M6 1L1 6L6 11" stroke="#0C2A50" stroke-linecap="round"></path>
                            </svg>
                        </span>
                    </div>

                    <div class="swiper-button-next swiper-recent-news-next">
                        <span class="icon">
                            <svg width="7" height="12" viewBox="0 0 7 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M1 11L6 6L1 1" stroke="#0C2A50" stroke-linecap="round"></path>
                            </svg>
                        </span>
                    </div>
                </div>
            <?php endif; ?>

            <?php if ( ! empty( $button ) ) : ?>
                <div class="d-flex justify-content-center mt-md-0 mt-50">
                    <?php eco_btn($button, 'eco-btn eco-btn_icon eco-btn_main mt-md-70 mr-sm-30 mb-sm-0'); ?>
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>