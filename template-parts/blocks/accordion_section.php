<?php
/*
 * Block Name: Accordion section Block
 * Slug:
 * Description:
 * Keywords:
 * Dependency:
 * Align: false
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

$description = get_field('description');
$title = get_field('title');
$repeater = get_field('repeater');
$block_name = 'eco-accordion_section';
// Create id attribute allowing for custom "anchor" value.
$id = $block_name . '-' . $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}
// Create class attribute allowing for custom "className" and "align" values.
$className   = array( $block_name );
$className[] = 'eco-section-element';
?>
<div class="<?php echo implode( ' ', $className ); ?>" id="<?php echo esc_attr( $id ); ?>">
    <div class="container">
        <div class="eco-block-center">
            <?php if ( ! empty( $title ) ) : ?>
                <h2 class="eco-block-title"><?php echo $title; ?></h2>
            <?php endif; ?>

            <?php if ( ! empty( $description ) ) : ?>
                <div class="eco-block-subtitle mb-lg-100 mb-md-60"><?php echo $description; ?></div>
            <?php endif; ?>
        </div>

        <?php if ( ! empty( $repeater ) ) : ?>
            <div class="row">
                <?php foreach( $repeater as $row ): ?>
                <?php $icon = $row['icon']; ?>
                <?php $title = $row['title']; ?>
                <?php $text = $row['text']; ?>
                <?php $button = $row['button']; ?>
                    <div class="col-lg-6">
                        <div class="eco-accordion values_item">
                            <div class="eco-accordion__header">
                                <div class="eco-accordion__header-icon">
                                    <img src="<?php echo $row ['icon']['url']; ?>" class="icon">
                                </div>
                                <h4 class="eco-accordion__header-title"><?php echo $title; ?></h4>
                                <button type="button" class="eco-btn eco-accordion__header-btn">
                                    <span></span>
                                    <span></span>
                                </button>
                            </div>

                            <div class="eco-accordion__body">
                                <?php echo wpautop( $text ); ?>
                                <?php eco_btn($button, 'eco-btn eco-btn_icon'); ?>
                                <div class="eco-accordion__body-bg"></div>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        <?php endif; ?>
    </div>
</div>





