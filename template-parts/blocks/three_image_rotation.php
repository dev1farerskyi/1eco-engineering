<?php
/*
 * Block Name: Three Image rotation Block
 * Slug:
 * Description:
 * Keywords:
 * Dependency:
 * Align: false
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

$title = get_field('title');
$text = get_field('text');
$images = get_field('images');

$block_name = 'eco-three_Image_rotation';
// Create id attribute allowing for custom "anchor" value.
$id = $block_name . '-' . $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}
// Create class attribute allowing for custom "className" and "align" values.
$className   = array( $block_name );
$className[] = 'eco-section-element';
?>
<div class="<?php echo implode( ' ', $className ); ?>" id="<?php echo esc_attr( $id ); ?>">
    <div class="container">
        <div class="eco-card-slider__wrap">
            <div class="row flex-column flex-lg-row justify-content-between align-items-lg-center align-items-start">
                <div class="col-xxl-5 col-lg-6">
                    <div class="eco-card-slider__content">
                        <?php if ( ! empty( $title ) ) : ?>
                            <h2 class="eco-block-title mb-lg-40 mb-20"><?php echo $title; ?></h2>
                        <?php endif ?>
                        <?php if ( ! empty( $text ) ) : ?>
                            <p><?php echo $text; ?></p>
                        <?php endif ?>
                    </div>
                </div>
                <?php if ( ! empty( $images ) ) : ?>
                    <div class="col-xxl-5 col-lg-6">
                        <div class="eco-card-slider__wrap">
                            <div class="swiper swiper-image-card">
                                <div class="swiper-wrapper">
                                    <?php foreach ($images as $image):?>
                                        <div class="swiper-slide">
                                            <div class="eco-card-slider__image">
                                                <div class="eco-card-slider__image-wrap">
                                                    <img src="<?php echo $image ['image']['url']; ?>" alt="image">
                                                </div>
                                            </div>
                                        </div>
                                    <?php endforeach; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
            </div>

            <div class="swiper-cont swiper-button-center">
                <div class="swiper-button-prev swiper-image-card-prev">
                    <span class="icon">
                        <svg width="7" height="12" viewBox="0 0 7 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M6 1L1 6L6 11" stroke="#0C2A50" stroke-linecap="round"></path>
                        </svg>
                    </span>
                </div>

                <div class="swiper-button-next swiper-image-card-next">
                    <span class="icon">
                        <svg width="7" height="12" viewBox="0 0 7 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M1 11L6 6L1 1" stroke="#0C2A50" stroke-linecap="round"></path>
                        </svg>
                    </span>
                </div>
            </div>
        </div>
    </div>
</div>










