<?php
/*
 * Block Name: Large quote Block
 * Slug:
 * Description:
 * Keywords:
 * Dependency:
 * Align: false
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

$quote = get_field('quote');
$name = get_field('name');

$block_name = 'eco-large_quote_block';
// Create id attribute allowing for custom "anchor" value.
$id = $block_name . '-' . $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}
// Create class attribute allowing for custom "className" and "align" values.
$className   = array( $block_name );
$className[] = 'eco-section-element';
?>
<div class="<?php echo implode( ' ', $className ); ?>" id="<?php echo esc_attr( $id ); ?>">
    <div class="container">
        <div class="container-small">
            <div class="eco-large_quote_block__content">
                <?php if ( ! empty( $quote ) ) : ?>
                    <h3 class="eco-large_quote_block__text"><?php echo $quote; ?></h3>
                <?php endif ?>
                <?php if ( ! empty( $name ) ) : ?>
                    <h4 class="eco-large_quote_block__name"><?php echo $name; ?></h4>
                <?php endif ?>
                <div class="eco-large_quote_block__content-back"></div>
            </div>
            <div class="eco-large_quote_block__author-bg"></div>
        </div>
    </div>
</div>













