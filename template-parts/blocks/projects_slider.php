<?php
/*
 * Block Name: Projects Slider Block
 * Slug:
 * Description:
 * Keywords:
 * Dependency:
 * Align: false
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

$description = get_field('description');
$title = get_field('title');
$button = get_field('button');

$slider = get_field('slider');
$block_name = 'eco-projects-slider';
// Create id attribute allowing for custom "anchor" value.
$id = $block_name . '-' . $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}
// Create class attribute allowing for custom "className" and "align" values.
$className = array($block_name);
$className[] = 'eco-section-element';
?>
<div class="<?php echo implode(' ', $className); ?>" id="<?php echo esc_attr($id); ?>">
    <div class="eco-projects__decor">
        <?php echo file_get_contents(get_template_directory_uri() . '/assets/img/back_main_svg.svg'); ?>
    </div>
    <div class="container">
        <div class="eco-projects-slider__head">
            <div class="row">
                <div class="col-lg-6">
                    <?php if (!empty($title)) : ?>
                        <h2 class="eco-block-title mb-10"><?php echo $title; ?></h2>
                    <?php endif ?>
                </div>
                <div class="col-lg-6">
                    <?php if (!empty($description)) : ?>
                        <p class="eco-block-subtitle mb-lg-30 mb-20 pb-5"><?php echo $description; ?></p>
                    <?php endif ?>
                    <?php eco_btn($button, 'eco-btn eco-btn_icon eco-btn_main mr-sm-30 mb-sm-0'); ?>
                </div>
            </div>
        </div>
    </div>

    <?php if (!empty($slider)) : ?>
        <div class="eco-projects-slider__wrap">
            <div class="swiper swiper-custom">
                <div class="swiper-wrapper">
                    <?php foreach ($slider as $row): ?>
                        <?php $image = $row['image']; ?>
                        <?php $title = $row['title']; ?>
                        <?php $text = $row['text']; ?>
                        <?php $small_text = $row['small_text']; ?>
                        <?php $button = $row['button']; ?>
                        <?php if ($image && $title && $text && $small_text): ?>
                            <div class="swiper-slide">
                                <div class="container">
                                    <div class="eco-projects-slider__item">
                                        <div class="eco-projects-slider__item-line"></div>
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="eco-projects-slider__item-image">
                                                    <img src="<?php echo $row ['image']['url']; ?>" alt="image">
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="eco-projects-slider__item-content">
                                                    <h3 class="eco-projects-slider__item-title"><?php echo $title; ?></h3>
                                                    <div class="eco-projects-slider__item-text">
                                                        <p><?php echo $text; ?></p>
                                                    </div>
                                                    <div class="eco-projects-slider__item-author"><?php echo $small_text; ?></div>
                                                    <?php eco_btn($button, 'eco-btn eco-btn_icon'); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endif; ?>
                    <?php endforeach; ?>
                </div>
            </div>

            <div class="swiper-cont">
                <div class="swiper-button-prev swiper-custom-prev"> 
                    <span class="icon">
                        <svg width="7" height="12" viewBox="0 0 7 12" fill="none" xmlns="http://www.w3.org/2000/svg">
							<path d="M6 1L1 6L6 11" stroke="#0C2A50" stroke-linecap="round"></path>
						</svg>
                    </span>
                </div>
                <div class="swiper-button-next swiper-custom-next">
                    <span class="icon">
                        <svg width="7" height="12" viewBox="0 0 7 12" fill="none" xmlns="http://www.w3.org/2000/svg">
							<path d="M1 11L6 6L1 1" stroke="#0C2A50" stroke-linecap="round"></path>
						</svg>
                    </span>
                </div>
            </div>
        </div>
    <?php endif; ?>
</div>







