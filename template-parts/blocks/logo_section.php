<?php
/*
 * Block Name: Logo section Block
 * Slug:
 * Description:
 * Keywords:
 * Dependency:
 * Align: false
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */


$title = get_field('title');
$logos = get_field('logos');

$block_name = 'eco-partners';

// Create id attribute allowing for custom "anchor" value.
$id = $block_name . '-' . $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}
// Create class attribute allowing for custom "className" and "align" values.
$className   = array( $block_name );
$className[] = 'eco-section-element';
?>
<div class="<?php echo implode( ' ', $className ); ?>" id="<?php echo esc_attr( $id ); ?>">
    <div class="container">
        <?php if ( ! empty( $logos ) ) : ?>
            <div class="eco-block-center">
                <?php if ( ! empty( $title ) ) : ?>
                    <h2 class="eco-block-title mb-lg-60 mb-40"><?php echo $title; ?></h2>
                <?php endif ?>
            </div>
            <div class="eco-partners__wrap">
                <div class="swiper swiper-partners">
                    <div class="swiper-wrapper">
                        <?php foreach ($logos as $logo):?>
                            <div class="swiper-slide">
                                <div class="eco-partners__item">
                                    <img src="<?php echo esc_url( $logo['url'] ); ?>" alt="image">
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        <?php endif; ?>
    </div>
</div>




