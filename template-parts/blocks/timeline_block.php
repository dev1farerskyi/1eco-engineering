<?php
/*
 * Block Name: Timeline Block
 * Slug:
 * Description:
 * Keywords:
 * Dependency:
 * Align: false
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

$title = get_field('title');
$years = get_field('years');


$block_name = 'eco-timeline_block';
// Create id attribute allowing for custom "anchor" value.
$id = $block_name . '-' . $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}
// Create class attribute allowing for custom "className" and "align" values.
$className   = array( $block_name );
$className[] = 'eco-section-element';
?>
<div class="<?php echo implode( ' ', $className ); ?>" id="<?php echo esc_attr( $id ); ?>">
    <div class="container">
        <?php if ( ! empty( $years ) ) : ?>
            <div class="eco-history__head">
                <div class="eco-history__head-content">
                    <?php if ( ! empty( $title ) ) : ?>
                        <div class="eco-history__head-title">
                            <h2 class="eco-block-title color-white mb-md-30 mb-0"><?php echo $title; ?></h2>
                            <div class="swiper-cont">
                                <div class="swiper-button-prev swiper-history-prev d-none d-md-flex">
                                    <span class="icon">
                                        <svg width="7" height="12" viewBox="0 0 7 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M6 1L1 6L6 11" stroke="#0C2A50" stroke-linecap="round"></path>
                                        </svg>
                                    </span>
                                </div>
                                <div class="swiper-button-next swiper-history-next d-none d-md-flex">
                                    <span class="icon">
                                        <svg width="7" height="12" viewBox="0 0 7 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M1 11L6 6L1 1" stroke="#0C2A50" stroke-linecap="round"></path>
                                        </svg>
                                    </span>
                                </div>
                            </div>
                        </div>
                    <?php endif ?>
                    <div class="eco-history__nav">
                        <div thumbsSlider="" class="swiper mySwiper swiper-history-nav">
                            <div class="swiper-wrapper">
                                <?php foreach ($years as $year):?>
                                    <div class="swiper-slide">
                                        <div class="eco-history__nav-item">
                                            <div class="eco-history__nav-item-circle"></div>
                                            <div class="eco-history__nav-item-year"><?php echo $year['year']; ?></div>
                                        </div>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="eco-history__body">
                <div class="swiper mySwiper2 swiper-history">
                    <div class="swiper-wrapper">
                        <?php foreach( $years as $row ): ?>
                            <?php $image = $row['image']; ?>
                            <?php $title = $row['title']; ?>
                            <?php $description = $row['description']; ?>
                            <?php if($image && $title && $description): ?>
                                <div class="swiper-slide">
                                    <div class="eco-history__slide">
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="eco-history__slide-image">
                                                    <div class="eco-history__slide-image-wrap">
                                                        <img src="<?php echo $row ['image']['url']; ?>" alt="image">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-5 col-lg-6">
                                                <div class="eco-history__slide-content">
                                                    <h3 class="eco-history__slide-year"><?php echo $title; ?></h3>
                                                    <div class="eco-history__slide-text">
                                                        <p><?php echo $description; ?></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        <?php endif; ?>
    </div>
</div>




















