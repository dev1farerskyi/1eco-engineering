<?php
/*
 * Block Name: Testimonials Block
 * Slug:
 * Description:
 * Keywords:
 * Dependency:
 * Align: false
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

$title = get_field('title');
$description = get_field('description');
$button = get_field('button');
$testimonials = get_field('testimonials');

$args = array(
    'post_type' => 'testimonials',
    'post_status' => 'publish',
    'post_per_page' => 3,
    'post__in' => $testimonials,
);
$testimonials_query = new WP_Query($args);

$block_name = 'eco-testimonials';
// Create id attribute allowing for custom "anchor" value.
$id = $block_name . '-' . $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}
// Create class attribute allowing for custom "className" and "align" values.
$className   = array( $block_name );
$className[] = 'eco-section-element';
?>
<div class="<?php echo implode( ' ', $className ); ?>" id="<?php echo esc_attr( $id ); ?>">
    <div class="container">
        <div class="row justify-content-center mb-40">
            <div class="col-xl-9">
                <div class="eco-block-center">
                    <?php if ( ! empty( $title ) ) : ?>
                        <h2 class="eco-block-title mb-15"><?php echo $title; ?></h2>
                    <?php endif; ?>

                    <?php if ( ! empty( $description ) ) : ?>
                        <div class="eco-block-subtitle"><?php echo $description; ?></div>
                    <?php endif; ?>

                    <div class="button-center testimonials-button">
                        <?php eco_btn($button, 'eco-btn eco-btn_icon eco-btn_border'); ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="eco-recent__head">
            <div></div>
            <div class="swiper-cont swiper-cont--desktop">
                <div class="swiper-button-prev swiper-recent-news-prev">
                    <span class="icon">
                        <svg width="7" height="12" viewBox="0 0 7 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M6 1L1 6L6 11" stroke="#0C2A50" stroke-linecap="round"></path>
                        </svg>
                    </span>
                </div>
                <div class="swiper-button-next swiper-recent-news-next">
                    <span class="icon">
                        <svg width="7" height="12" viewBox="0 0 7 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M1 11L6 6L1 1" stroke="#0C2A50" stroke-linecap="round"></path>
                        </svg>
                    </span>
                </div>
            </div>
        </div>

        <?php if ( $testimonials_query->have_posts() ) : ?>
            <div class="eco-reviews__wrap">
                <div class="swiper swiper-recent-news">
                    <div class="swiper-wrapper">
                        <?php while ($testimonials_query->have_posts()) : $testimonials_query->the_post();
                            $name_and_title = get_field( 'name_and_title', get_the_ID() );
                            $quote = get_field( 'quote', get_the_ID() );
                            $button = get_field( 'button', get_the_ID() ); ?>

                            <div class="swiper-slide">
                                <div class="eco-reviews__item">
                                    <div class="eco-reviews__item-wrap">
                                        <div class="eco-reviews__item-bg"></div>
                                        <div class="eco-reviews__item-text"><?php echo wp_kses_post( $quote ); ?></div>
                                        <h6 class="eco-reviews__item-author"><?php echo esc_html( $name_and_title ); ?></h6>
                                        <div class="eco-reviews__item-link">
                                            <?php eco_btn($button, 'eco-btn eco-btn_icon'); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endwhile; wp_reset_postdata(); ?>
                    </div>
                </div>
            </div>

            <div class="swiper-cont swiper-cont--mobile">
                <div class="swiper-button-prev swiper-recent-news-prev">
                    <span class="icon">
                        <svg width="7" height="12" viewBox="0 0 7 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M6 1L1 6L6 11" stroke="#0C2A50" stroke-linecap="round"></path>
                        </svg>
                    </span>
                </div>
                <div class="swiper-button-next swiper-recent-news-next">
                    <span class="icon">
                        <svg width="7" height="12" viewBox="0 0 7 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M1 11L6 6L1 1" stroke="#0C2A50" stroke-linecap="round"></path>
                        </svg>
                    </span>
                </div>
            </div>
        <?php endif; ?>
    </div>
</div>











