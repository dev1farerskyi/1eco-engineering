<?php
/*
 * Block Name: Projects Block
 * Slug:
 * Description:
 * Keywords:
 * Dependency:
 * Align: false
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

$block_name = 'eco-more_real_projects';
// Create id attribute allowing for custom "anchor" value.
$id = $block_name . '-' . $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}
// Create class attribute allowing for custom "className" and "align" values.
$className   = array( $block_name );
$className[] = 'eco-section-element';

// Get terms
$services = get_terms(array(
    'taxonomy' => 'projects_services',
    'hide_empty' => false,
));

$industries = get_terms(array(
    'taxonomy' => 'projects_industry',
    'hide_empty' => false,
));

// Projects query
$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
$args = array(
    'post_type' => 'projects',
    'post_status' => 'publish',
    'posts_per_page' => 9,
    'paged' => $paged,
);

$projects_query = new WP_Query( $args );
?>
<div class="<?php echo implode( ' ', $className ); ?>" id="<?php echo esc_attr( $id ); ?>">
    <div class="container">
        <div class="eco-filter eco-news__filter">
            <form action="#" class="js-sorting-projects">
                <div class="row">
                    <?php if ( ! empty( $industries ) ) : ?>
                        <div class="col-lg-3">
                            <div class="eco-field-form field_select">
                                <select class="select2" name="industry" data-placeholder="All Industries" style="width: 100%">
                                    <option value="all"><?php esc_html_e('All Industries', ECO_PREFIX); ?></option>

                                    <?php foreach ( $industries as $industry ) : ?>
                                        <option value="<?php echo $industry->term_id; ?>"><?php echo $industry->name; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                    <?php endif; ?>

                    <?php if ( ! empty( $services ) ) : ?>
                        <div class="col-lg-3">
                            <div class="eco-field-form field_select">
                                <select class="select2" name="services" data-placeholder="All Services" style="width: 100%">
                                    <option value="all"><?php esc_html_e('All Services', ECO_PREFIX); ?></option>

                                    <?php foreach ( $services as $service ) : ?>
                                        <option value="<?php echo $service->term_id; ?>"><?php echo $service->name; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                    <?php endif; ?>

                    <div class="col-lg-6">
                        <div class="d-flex eco-filter__row">
                            <div class="eco-field-form field_search">
                                <input type="text" name="s" placeholder="<?php esc_attr_e('Search by keyword', ECO_PREFIX); ?>">
                            </div>

                            <button type="submit" class="eco-btn eco-btn_icon eco-btn_main w-100">
                                <?php esc_html_e('Search', ECO_PREFIX); ?>
                                <span class="icon"></span>
                            </button>
                        </div>
                    </div>
                </div>
            </form>
        </div>

        <?php if ( $projects_query->have_posts() ) : ?>
            <div class="row js-posts-wrapper">
                <?php while ( $projects_query->have_posts() ) : $projects_query->the_post(); ?>
                    <div class="col-lg-4 col-md-6">
                        <?php get_template_part('template-parts/content'); ?>
                    </div>
                <?php endwhile; wp_reset_postdata(); ?>

                <div class="eco-pagination mb-150">
                    <div class="d-flex eco-pagination__list">
                        <?php
                        $big = 999999999;
                        echo paginate_links( array(
                            'base' => str_replace( $big, '%#%', get_pagenum_link( $big ) ),
                            'format' => '?paged=%#%',
                            'current' => max( 1, get_query_var('paged') ),
                            'total' => $projects_query->max_num_pages,
                            'prev_next'    => True,
                            'prev_text'    => '<span class="icon">
								<svg width="7" height="12" viewBox="0 0 7 12" fill="none" xmlns="http://www.w3.org/2000/svg">
									<path d="M6 1L1 6L6 11" stroke="#0C2A50" stroke-linecap="round"></path>
								</svg>
							</span>' . __('Previous'),
                            'next_text'    => __('Next') . '<span class="icon">
                        <svg width="7" height="12" viewBox="0 0 7 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M1 11L6 6L1 1" stroke="#0C2A50" stroke-linecap="round"></path>
                        </svg>
                    </span>',
                        ) ); ?>
                    </div>
                </div>
            </div>
        <?php endif; ?>
    </div>
</div>





