<?php
/*
 * Block Name: News Block
 * Slug:
 * Description:
 * Keywords:
 * Dependency:
 * Align: false
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

$block_name = 'eco-more_real_projects';
// Create id attribute allowing for custom "anchor" value.
$id = $block_name . '-' . $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}
// Create class attribute allowing for custom "className" and "align" values.
$className   = array( $block_name );
$className[] = 'eco-section-element';

// Posts query
$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
$args = array(
    'post_type' => 'post',
    'post_status' => 'publish',
    'posts_per_page' => 9,
    'paged' => $paged,
);

$post_query = new WP_Query( $args );
?>
<div class="<?php echo implode( ' ', $className ); ?>" id="<?php echo esc_attr( $id ); ?>">
    <div class="container">
        <?php if ( $post_query->have_posts() ) : ?>
            <div class="row">
                <?php while ( $post_query->have_posts() ) : $post_query->the_post(); ?>
                    <div class="col-lg-4 col-md-6">
                        <?php get_template_part('template-parts/content'); ?>
                    </div>
                <?php endwhile; wp_reset_postdata(); ?>

                <div class="eco-pagination mb-150">
                    <div class="d-flex eco-pagination__list">
                        <?php
                        $big = 999999999;
                        echo paginate_links( array(
                            'base' => str_replace( $big, '%#%', get_pagenum_link( $big ) ),
                            'format' => '?paged=%#%',
                            'current' => max( 1, get_query_var('paged') ),
                            'total' => $post_query->max_num_pages,
                            'prev_next'    => True,
                            'prev_text'    => '<span class="icon">
								<svg width="7" height="12" viewBox="0 0 7 12" fill="none" xmlns="http://www.w3.org/2000/svg">
									<path d="M6 1L1 6L6 11" stroke="#0C2A50" stroke-linecap="round"></path>
								</svg>
							</span>' . __('Previous'),
                            'next_text'    => __('Next') . '<span class="icon">
                                <svg width="7" height="12" viewBox="0 0 7 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M1 11L6 6L1 1" stroke="#0C2A50" stroke-linecap="round"></path>
                                </svg>
                            </span>',
                        ) ); ?>
                    </div>
                </div>
            </div>
        <?php endif; ?>
    </div>
</div>





