<?php
/*
 * Block Name: Content with the list Block
 * Slug:
 * Description:
 * Keywords:
 * Dependency:
 * Align: false
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

$title = get_field('title');
$button = get_field('button');
$list = get_field('list');
$block_name = 'eco-content_with_the_list';
// Create id attribute allowing for custom "anchor" value.
$id = $block_name . '-' . $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}
// Create class attribute allowing for custom "className" and "align" values.
$className   = array( $block_name );
$className[] = 'eco-section-element';
?>

<div class="<?php echo implode( ' ', $className ); ?>" id="<?php echo esc_attr( $id ); ?>">
    <div class="container">
        <div class="row">
            <div class="col-lg-5">
                <?php if ( ! empty( $title ) ) : ?>
                    <h2 class="eco-expertise__title"><?php echo $title; ?></h2>
                <?php endif ?>
                <?php eco_btn($button, 'eco-btn eco-btn_icon eco-btn_main mr-sm-30 mb-sm-0 mb-25'); ?>
            </div>
            <div class="col-lg-7">
                <?php if ( ! empty( $list ) ) : ?>
                    <div class="eco-expertise__list"><?php echo $list; ?></div>
                <?php endif ?>
            </div>
        </div>
    </div>
</div>













