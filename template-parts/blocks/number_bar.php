<?php
/*
 * Block Name: Number Bar Block
 * Slug:
 * Description:
 * Keywords:
 * Dependency:
 * Align: false
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

$repeater = get_field('repeater');

$block_name = 'eco-number_bar';
// Create id attribute allowing for custom "anchor" value.
$id = $block_name . '-' . $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}
// Create class attribute allowing for custom "className" and "align" values.
$className   = array( $block_name );
$className[] = 'eco-section-element';
?>
<div class="<?php echo implode( ' ', $className ); ?>" id="<?php echo esc_attr( $id ); ?>">
    <div class="container">
        <?php if ( ! empty( $repeater ) ) : ?>
            <div class="eco-statistics__wrap">
                <div class="eco-statistics__list">
                    <?php foreach( $repeater as $row ): ?>
                        <?php $number = $row['number']; ?>
                        <?php $after_number = $row['after_number']; ?>
                            <?php $description = $row['description']; ?>
                            <div class="eco-statistics__col">
                                <div class="eco-statistics__col-number-count">
                                    <?php if($number): ?>
                                        <h2 class="eco-statistics__col-number">
                                            <?php echo esc_html( $number ); ?>
                                        </h2>
                                    <?php endif; ?>
                                    <?php if($after_number): ?>
                                        <h2 class="eco-statistics__col-number-after"><?php echo $after_number; ?></h2>
                                    <?php endif; ?>
                                </div>
                                <p class="eco-statistics__col-text"><?php echo esc_html( $description ); ?></p>
                            </div>
                    <?php endforeach; ?>
                </div>
            </div>
        <?php endif; ?>
    </div>
</div>










