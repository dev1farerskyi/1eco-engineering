<?php
/*
 * Block Name: Content with image Block
 * Slug:
 * Description:
 * Keywords:
 * Dependency:
 * Align: false
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

$title = get_field('title');
$text = get_field('text');
$image = get_field('image');
$button_1 = get_field('button_1');
$button_2 = get_field('button_2');

$block_name = 'eco-content_with_image';
// Create id attribute allowing for custom "anchor" value.
$id = $block_name . '-' . $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}
// Create class attribute allowing for custom "className" and "align" values.
$className   = array( $block_name );
$className[] = 'eco-section-element';
?>
<div class="<?php echo implode( ' ', $className ); ?>" id="<?php echo esc_attr( $id ); ?>">
    <div class="container">
        <div class="row flex-column-reverse flex-md-row">
            <div class="col-md-6">
                <div class="eco-text-card__wrap">
                    <div class="eco-text-card__content">
                        <?php if ( ! empty( $title ) ) : ?>
                            <h2 class="eco-block-title"><?php echo $title; ?></h2>
                        <?php endif ?>
                        <?php if ( ! empty( $text ) ) : ?>
                            <p><?php echo $text; ?></p>
                        <?php endif ?>
                        <div class="eco-text-card__container-btn">
                            <?php eco_btn($button_1, 'eco-btn eco-btn_icon eco-btn_main mr-sm-30 mb-sm-0 mb-25'); ?>
                            <?php eco_btn($button_2, 'eco-btn eco-btn_icon eco-btn_border'); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <?php if ( ! empty( $image ) ) : ?>
                    <div class="eco-text-card__image">
                        <div class="eco-text-card__image-wrap">
                            <img src="<?php echo esc_url($image['url']); ?>" alt="image">
                        </div>
                    </div>
                <?php endif ?>
            </div>
        </div>
    </div>
</div>






















