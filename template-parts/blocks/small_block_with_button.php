<?php
/*
 * Block Name: Small block with button Block
 * Slug:
 * Description:
 * Keywords:
 * Dependency:
 * Align: false
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

$description = get_field('description');
$text = get_field('text');
$button = get_field('button');
$block_name = 'eco-small_block_with_button';
// Create id attribute allowing for custom "anchor" value.
$id = $block_name . '-' . $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}
// Create class attribute allowing for custom "className" and "align" values.
$className   = array( $block_name );
$className[] = 'eco-section-element';

$args = array(
    'classes' => implode(' ', $className ),
    'id' => $id,
    'text' => $text,
    'description' => $description,
    'button' => $button,
);

get_template_part( 'template-parts/elements/small-block', null, $args );
