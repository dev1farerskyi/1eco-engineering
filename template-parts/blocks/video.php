<?php
/*
 * Block Name: Video Block
 * Slug:
 * Description:
 * Keywords:
 * Dependency:
 * Align: false
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

$cover_image = get_field('cover_image');
$video_url = get_field('video_url', false, false);

$block_name = 'eco-video';
// Create id attribute allowing for custom "anchor" value.
$id = $block_name . '-' . $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}
// Create class attribute allowing for custom "className" and "align" values.
$className   = array( $block_name );
$className[] = 'eco-section-element';
?>
<div class="<?php echo implode(' ', $className) ?>" id="<?php echo $id; ?>">
    <div class="container">
        <div class="container-small">
            <div class="eco-video__wrap">
                <?php if ( ! empty( $cover_image ) ) : ?>
                    <img class="eco-image-video-block__img" src="<?php echo $cover_image['url']; ?>" alt="<?php echo $cover_image['alt']; ?>">
                <?php endif ?>

                <a class="eco-image-video-block__button js-magnific-video" href="<?php echo $video_url; ?>">
                    <svg width="21" height="24" viewBox="0 0 21 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M3.84822 1.49791L18.9301 11.1336C19.9232 11.768 19.8936 13.2283 18.8757 13.8221L3.79383 22.6198C2.74354 23.2325 1.42453 22.4749 1.42453 21.259V2.82556C1.42453 1.58109 2.79951 0.827909 3.84822 1.49791Z" fill="#0C2A50" stroke="#0C2A50" stroke-width="1.15094" stroke-linecap="round"/>
                    </svg>
                </a>
            </div>
        </div>
    </div>
</div>
