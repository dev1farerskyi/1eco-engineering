<?php
/*
 * Block Name: Content with the form Block
 * Slug:
 * Description:
 * Keywords:
 * Dependency:
 * Align: false
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */
$title = get_field('title');
$description = get_field('description');
$text = get_field('text');
$adress = get_field('adress');
$phone_number = get_field('phone_number');
$contact_form = get_field('contact_form');

$block_name = 'eco-content_with_the_form';
// Create id attribute allowing for custom "anchor" value.
$id = $block_name . '-' . $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}
// Create class attribute allowing for custom "className" and "align" values.
$className   = array( $block_name );
$className[] = 'eco-section-element';
?>
<div class="<?php echo implode( ' ', $className ); ?>" id="<?php echo esc_attr( $id ); ?>">
    <div class="container">
        <div class="row">
            <div class="col-lg-5">
                <?php if ( ! empty( $title ) ) : ?>
                    <h2 class="eco-block-title eco-contact-main__title">
                        <?php echo $title; ?>
                    </h2>
                <?php endif ?>
                <?php if ( ! empty( $text ) ) : ?>
                    <p class="eco-contact-main__text">
                        <?php echo $text; ?>
                    </p>
                <?php endif ?>
                <div class="eco-contact-main__info">
                    <?php if ( ! empty( $adress ) ) : ?>
                        <div class="eco-contact-main__address">
                            <img class="eco-contact-main__info__icon" src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/img/address-icon.svg" alt="address">
                            <div>
                                <?php echo $adress; ?>
                            </div>
                        </div>
                    <?php endif ?>
                    <?php if ( ! empty( $phone_number ) ) : ?>
                    <div>
                        <img class="eco-contact-main__info__icon" src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/img/tel-icon.svg" alt="telIcon">
                        <a class="eco-contact-main__tel" href="tel:<?php echo $phone_number; ?>0"><?php echo $phone_number; ?></a>
                    </div>
                    <?php endif ?>
                </div>
            </div>
            <div class="col-lg-7">
                <?php if ( ! empty( $contact_form ) ) : ?>
                    <?php echo do_shortcode('[gravityform id="' . $contact_form['id'] . '" ajax="true" title="false" description="false"]'); ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>










