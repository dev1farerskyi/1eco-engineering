<?php

/*
 * Block Name: Background Container
 * Slug:
 * Description:
 * Keywords:
 * Align: true
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

$style = get_field('style');
$negative_position = get_field('negative_position');
$background_position = get_field('background_position');
$negative_background_position = get_field('negative_background_position');

$block_name = 'eco-background-container';

// Create id attribute allowing for custom "anchor" value.
$id = $block_name . '-' . $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = array();

$className[] = $block_name;
if (!empty($block['className'])) {
    $className[] = $block['className'];
}
if (!empty($block['align'])) {
    $className[] = 'align-' . $block['align'];
}
if (!empty($is_preview)) $className[] = $block_name . '_is-preview';

$className[] = 'eco-section-element';
$className[] = $block_name . '_' . $style;
?>

<style>
    <?php if ( ! empty( $negative_position ) ) : ?>
        <?php if (!empty($negative_background_position)) : ?>
            <?php echo '#' . $id . ' .eco-background-container__bg {top:-' . $negative_background_position . 'px}' ?>
        <?php endif; ?>
    <?php else : ?>
        <?php if (!empty($background_position)) : ?>
            <?php echo '#' . $id . ' .eco-background-container__bg {top:' . $background_position . 'px}' ?>
        <?php endif; ?>
    <?php endif; ?>
</style>

<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr(trim(implode(' ', $className))) ?>">
    <div class="eco-background-container__bg"></div>

    <div class="eco-background-container__wrap">
        <InnerBlocks/>
    </div>
</div>
