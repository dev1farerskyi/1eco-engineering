<?php
/*
 * Block Name: Text Section
 * Slug:
 * Description:
 * Keywords:
 * Dependency:
 * Align: false
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

$title = get_field('title');
$text = get_field('text');

$block_name = 'eco-text_section';
// Create id attribute allowing for custom "anchor" value.
$id = $block_name . '-' . $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}
// Create class attribute allowing for custom "className" and "align" values.
$className   = array( $block_name );
$className[] = 'eco-section-element';
?>
<div class="<?php echo implode( ' ', $className ); ?>" id="<?php echo esc_attr( $id ); ?>">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-9 col-lg-11">
                <div class="eco-block-center">
                    <?php if ( ! empty( $title ) ) : ?>
                        <h2 class="eco-block-title"><?php echo $title; ?></h2>
                    <?php endif ?>
                    <?php if ( ! empty( $text ) ) : ?>
                        <div class="eco-block-subtitle"><?php echo $text; ?></div>
                    <?php endif ?>
                </div>
            </div>
        </div>
    </div>
</div>










