<?php
/*
 * Block Name: Awards Section Block
 * Slug:
 * Description:
 * Keywords:
 * Dependency:
 * Align: false
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

$title = get_field('title');
$gallery_repeater = get_field('gallery_repeater');


$block_name = 'eco-awards_section';
// Create id attribute allowing for custom "anchor" value.
$id = $block_name . '-' . $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}
// Create class attribute allowing for custom "className" and "align" values.
$className = array($block_name);
$className[] = 'eco-section-element';
?>
<div class="<?php echo implode(' ', $className); ?>" id="<?php echo esc_attr($id); ?>">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-9">
                <div class="eco-block-center">
                    <?php if (!empty($title)) : ?>
                        <h2 class="eco-block-title mb-lg-70 mb-40"><?php echo $title; ?></h2>
                    <?php endif ?>
                </div>
            </div>
        </div>

        <?php if (!empty($gallery_repeater)) : ?>
            <div class="row align-items-stretch">
                <div class="col-lg-5">
                    <div class="d-block d-lg-none">
                        <div class="eco-info-tabs__filter">
                            <div class="eco-field-form field_select">
                                <select class="select2 js-change-awards" style="width: 100%">
                                    <?php foreach ($gallery_repeater as $key => $row): ?>
                                        <option value="<?php echo 'tab-' . $key; ?>"><?php echo $row['gallery_name']; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="eco-info-tabs__list d-none d-lg-block">
                        <?php foreach ($gallery_repeater as $index => $row): ?>
                            <?php $gallery_name = $row['gallery_name']; ?>
                            <?php if ($gallery_name): ?>
                                <a href="javascript:void(0)" data-tab="<?php echo 'tab-' . $index; ?>"
                                   class="eco-info-tabs__link <?php echo $index === 0 ? 'active' : ''; ?>">
                                    <span class="icon">
                                        <span class="icon-arrow">
                                            <svg width="25" height="8" viewBox="0 0 25 8" fill="none"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <path d="M24.3536 4.35355C24.5488 4.15829 24.5488 3.84171 24.3536 3.64645L21.1716 0.464466C20.9763 0.269204 20.6597 0.269204 20.4645 0.464466C20.2692 0.659728 20.2692 0.976311 20.4645 1.17157L23.2929 4L20.4645 6.82843C20.2692 7.02369 20.2692 7.34027 20.4645 7.53553C20.6597 7.7308 20.9763 7.7308 21.1716 7.53553L24.3536 4.35355ZM24 3.5L0 3.5V4.5L24 4.5V3.5Z"
                                                      fill="#B7E588"></path>
                                            </svg>
                                        </span>
                                    </span>
                                    <span class="eco-info-tabs__link-text">
                                    <?php echo $gallery_name; ?>
                                </span>
                                </a>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </div>
                </div>
                <div class="col-lg-7">
                    <div class="eco-awards_section__content">
                        <?php foreach ($gallery_repeater as $index => $row):
                            $images = $row['gallery_images']; ?>
                            <?php if ($images): ?>
                                <div class="eco-info-tabs__box
                                    <?php echo $index === 0 ? 'active' : ''; ?>" id="<?php echo 'tab-' . $index; ?>">
                                    <div class="row">
                                        <?php foreach ($images as $image): ?>
                                            <div class="col-lg-6 col-md-6">
                                                <div class="eco-info-tabs__image-cont">
                                                    <img src="<?php echo $image['sizes']['thumbnail']; ?>"
                                                         alt="<?php echo $image['alt']; ?>"/>
                                                    <p><?php echo $image['caption']; ?></p>
                                                </div>
                                            </div>
                                        <?php endforeach; ?>
                                    </div>
                                </div>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        <?php endif ?>
    </div>
</div>

















