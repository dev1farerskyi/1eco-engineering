<?php
/*
 * Block Name: Two column content Block
 * Slug:
 * Description:
 * Keywords:
 * Dependency:
 * Align: false
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

$repeater = get_field('repeater');

$block_name = 'eco-two_column_content';
// Create id attribute allowing for custom "anchor" value.
$id = $block_name . '-' . $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}
// Create class attribute allowing for custom "className" and "align" values.
$className   = array( $block_name );
$className[] = 'eco-section-element';
?>
<div class="<?php echo implode( ' ', $className ); ?>" id="<?php echo esc_attr( $id ); ?>">
    <div class="container">
        <div class="container-small">
            <?php if ( ! empty( $repeater ) ) : ?>
                <div class="eco-two_column_content__content">
                    <div class="row">
                        <?php foreach( $repeater as $row ): ?>
                            <?php $title = $row['title']; ?>
                            <?php $text = $row['text']; ?>
                            <?php if($title && $text): ?>
                                <div class="col-lg-6 col-sm-12">
                                    <div class="eco-two_column_content__column">
                                        <h3><?php echo esc_html( $title ); ?></h3>
                                        <div class="eco-two_column_content__text">
                                            <?php echo $text; ?>
                                        </div>
                                    </div>
                                </div>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>











