<?php
/**
 * Position content
 */

$post_id = get_the_ID();
$job_type = get_field( 'job_type', $post_id );
$description = get_field( 'description', $post_id );
?>

<div class="eco-accordion mb-lg-40 mb-30">
    <div class="eco-accordion__header">
        <?php if ( ! empty( $job_type ) ) : ?>
            <div class="eco-accordion__header-type">
                <?php echo esc_html( $job_type ); ?>
            </div>
        <?php endif; ?>

        <?php the_title('<h4 class="eco-accordion__header-title">', '</h4>'); ?>

        <button type="button" class="eco-btn eco-accordion__header-btn">
            <span></span>
            <span></span>
        </button>
    </div>

    <div class="eco-accordion__body">
        <div class="row">
            <div class="col-lg-9">
                <?php echo wpautop( $description ); ?>

                <a href="#" class="eco-btn eco-btn_icon">
                    Apply online
                    <span class="icon"></span>
                </a>
            </div>
        </div>
        <div class="eco-accordion__body-bg"></div>
    </div>
</div>
