<?php
/**
 * Post content
 */

$post_id = get_the_ID();
$post_type = get_post_type($post_id);
$classes = ! empty( $args['class'] ) ? $args['class'] : '';
?>

<div class="eco-news-card <?php echo esc_attr( $classes ); ?>">
    <div class="eco-news-card__wrap">
        <div class="eco-news-card__image">
            <?php the_post_thumbnail(); ?>
        </div>

        <div class="eco-news-card__content">
            <?php
            $services = get_the_terms($post_id, 'projects_services');
            $industries = get_the_terms($post_id, 'projects_industry');
            $categories = get_the_category();
            ?>
            <ul class="eco-news-card__tags">
                <?php if ( ! empty( $categories ) ) : ?>
                    <li>
                        <?php foreach ( $categories as $category ) : ?>
                            <a href="<?php echo get_term_link($category, 'category'); ?>"><?php echo $category->name; ?></a>
                        <?php endforeach; ?>
                    </li>
                <?php endif; ?>


                <?php if ( ! empty( $services ) ) : ?>
                    <li>
                        <?php foreach ( $services as $service ) : ?>
                            <a href="<?php echo get_term_link($service, 'projects_services'); ?>"><?php echo $service->name; ?></a>
                        <?php endforeach; ?>
                    </li>
                <?php endif; ?>

                <?php if ( ! empty( $industries ) ) : ?>
                    <li>
                        <?php foreach ( $industries as $key => $industry ) : ?>
                            <a href="<?php echo get_term_link($industry, 'projects_industry'); ?>"><?php echo $industry->name; ?></a>
                        <?php endforeach; ?>
                    </li>
                <?php endif; ?>
            </ul>

            <?php the_title('<h5 class="eco-news-card__title">', '</h5>'); ?>

            <div class="eco-news-card__text">
                <?php the_excerpt(); ?>
            </div>

            <a href="<?php the_permalink(); ?>" class="eco-btn eco-btn_icon">
                <?php esc_html_e('Learn More', ECO_PREFIX); ?>
                <span class="icon"></span>
            </a>
        </div>
    </div>
</div>
