<?php
/**
 * Share links
 */
?>

<div class="nt-article__share">
    <ul class="eco-social social_line mt-20 pt-5">
        <li>
            <a href="#" data-share="mailto:?subject=<?php the_title(); ?>&body=<?php the_permalink(); ?>" target="_blank">
                <img src="<?php echo get_template_directory_uri() . '/assets/img/mail_green.svg'; ?>" alt="email">
            </a>
        </li>
        <li>
            <a href="#" data-copy="<?php the_permalink(); ?>" class="js-copy" title="Copy link">
                <img src="<?php echo get_template_directory_uri() . '/assets/img/share_green.svg'; ?>" alt="twitter">
            </a>
        </li>
        <li>
            <a href="#" data-share="https://twitter.com/intent/tweet?text=<?php echo urlencode(get_the_title()); ?>&url=<?php the_permalink(); ?>">
                <img src="<?php echo get_template_directory_uri() . '/assets/img/twitter_green.svg'; ?>" alt="twitter">
            </a>
        </li>
        <li>
            <a href="#" data-share="https://www.facebook.com/sharer/sharer.php?u=<?php echo urlencode(get_permalink()) ?>">
                <img src="<?php echo get_template_directory_uri() . '/assets/img/facebook_green.svg'; ?>" alt="facebook">
            </a>
        </li>
        <li>
            <a href="#" data-share="https://www.linkedin.com/sharer/sharer.php?u=<?php echo urlencode(get_permalink()) ?>">
                <img src="<?php echo get_template_directory_uri() . '/assets/img/linkedin_green.svg'; ?>" alt="linkedin">
            </a>
        </li>
    </ul>
</div>
