<?php
get_header();

$button = get_field('button');
$blurb_description = get_field('blurb_description');
$repeater = get_field('repeater');

$services = get_the_terms(get_the_ID(), 'projects_services');
$industries = get_the_terms(get_the_ID(), 'projects_industry');

if ( have_posts() ) :
    while ( have_posts() ) : the_post(); ?>
        <div class="eco-hero eco-section-element">
            <div class="eco-hero__wrap">
                <div class="container">
                    <div class="eco-hero__content">
                        <div class="row">
                            <div class="col-lg-6 col-md-6">
                                <ul class="eco-hero__categories">
                                    <?php if ( ! empty( $services ) ) : ?>
                                        <?php foreach ( $services as $service ) : ?>
                                            <li>
                                                <a href="<?php echo get_term_link($service, 'projects_services'); ?>"><?php echo $service->name; ?></a>
                                            </li>
                                        <?php endforeach; ?>
                                    <?php endif; ?>

                                    <?php if ( ! empty( $industries ) ) : ?>
                                        <?php foreach ( $industries as $key => $industry ) : ?>
                                            <li>
                                                <a href="<?php echo get_term_link($industry, 'projects_industry'); ?>"><?php echo $industry->name; ?></a>
                                            </li>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </ul>

                                <?php the_title('<h1 class="eco-hero__title">', '</h1>'); ?>

                                <ul class="eco-social social_line mt-35 pt-5">
                                    <?php eco_btn($button, 'eco-btn eco-btn_icon eco-btn_main mr-sm-30 mb-sm-0 mb-25'); ?>
                                </ul>
                            </div>

                            <?php if ( ! empty( $blurb_description ) ) : ?>
                                <div class="col-lg-6 col-md-6">
                                    <div class="eco-hero__text">
                                        <?php echo wpautop( $blurb_description ); ?>
                                    </div>
                                </div>
                            <?php endif ?>
                        </div>

                    </div>
                </div>
            </div>

            <?php if ( ! empty( $repeater ) ) : ?>
                <div class="eco-statistics line_white position_left pos_top decoration_none">
                    <div class="container">
                        <div class="eco-statistics__wrap">
                            <div class="eco-statistics__list">
                                <?php foreach( $repeater as $row ): ?>
                                    <?php $not_number = $row['not_number']; ?>
                                    <?php $number = $row['number']; ?>
                                    <?php $description = $row['description']; ?>
                                    <?php if($description): ?>
                                        <div class="eco-statistics__col">
                                            <div class="eco-statistics__col-number-count">
                                                <?php if($not_number): ?>
                                                    <h2 class="eco-statistics__col-number-after"><?php echo $not_number; ?></h2>
                                                <?php endif; ?>

                                                <?php if($number): ?>
                                                    <h2 class="eco-statistics__col-number">
                                                        <?php echo $number; ?>
                                                    </h2>
                                                <?php endif; ?>
                                            </div>

                                            <div class="eco-statistics__col-text"><?php echo $description; ?></div>
                                        </div>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
        </div>

        <?php the_content(); ?>
    <?php endwhile;
endif; ?>


<?php get_footer(); ?>