<?php
/**
 * Footer template.
 *
 * @package default
 * @since 1.0.0
 *
 */

$copyright_title = get_field('copyright_title', 'options');
$copyright_text = get_field('copyright_text', 'options');
$copyright_link_1 = get_field('copyright_link_1', 'options');
$copyright_link_2 = get_field('copyright_link_2', 'options');
$footer_logo = get_field('footer_logo', 'options');
$subscribe_title = get_field('subscribe_title', 'options');
$subscribe_text = get_field('subscribe_text', 'options');
$socials = get_field('socials', 'options');
$button_subscribe = get_field('button_subscribe', 'options');
?>

</main>

<footer class="eco-footer">
    <div class="eco-footer__bg-circle"></div>
    <div class="eco-footer__decor">
        <?php echo file_get_contents(get_template_directory_uri() . '/assets/img/footer_background.svg'); ?>
    </div>

    <div class="container">
        <div class="eco-footer__main">
            <?php if ($footer_logo): ?>
                <a href="<?php echo home_url('/'); ?>" class="eco-footer__logo">
                    <img src="<?php echo esc_url($footer_logo['url']); ?>" alt="image">
                </a>
            <?php endif ?>

            <div class="row">
                <div class="col-xl-7 col-lg-6">
                    <div class="row">
                        <div class="col-6">
                            <h6 class="eco-footer__subtitle"><?php esc_html_e('Company', 'eco'); ?></h6>
                            <?php wp_nav_menu(
                                array(
                                    'container' => '',
                                    'menu_class' => 'eco-footer__menu',
                                    'menu' => 'footer-menu',
                                    'theme_location' => 'footer-menu',
                                    'depth' => 1,
                                )
                            ); ?>
                        </div>
                        <div class="col-6">
                            <?php if (!empty($socials)) : ?>
                                <h6 class="eco-footer__subtitle"><?php esc_html_e('Follow us', 'eco'); ?></h6>
                                <ul class="eco-social">
                                    <?php foreach ($socials as $row): ?>
                                        <?php $image = $row['image']; ?>
                                        <?php $link = $row['link']; ?>
                                        <?php $name = $row['name']; ?>
                                        <?php if ($image && $link && $name): ?>
                                            <li>
                                                <a target="_blank" href="<?php echo esc_url($link); ?>">
                                                    <img class="eco-social__image" src="<?php echo esc_url($image['url']); ?>" alt="">
                                                    <?php echo $name; ?>
                                                </a>
                                            </li>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                </ul>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
                <div class="col-xl-5 col-lg-6">
                    <div class="eco-footer__subscribe">
                        <?php if (!empty($subscribe_title)) : ?>
                            <h3 class="eco-footer__subscribe-title"><?php echo $subscribe_title; ?></h3>
                        <?php endif; ?>

                        <?php if (!empty($subscribe_text)) : ?>
                            <?php echo wpautop( $subscribe_text ); ?>
                        <?php endif; ?>

                        <?php eco_btn($button_subscribe, 'eco-btn eco-btn_icon eco-btn_main mr-sm-30 mb-sm-0 mb-25'); ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="eco-footer__bottom">
            <?php wp_nav_menu(
                array(
                    'container' => '',
                    'menu_class' => 'eco-footer__submenu d-block d-lg-none',
                    'menu' => 'footer-menu-2',
                    'theme_location' => 'footer-menu',
                    'depth' => 1,
                )
            ); ?>

            <div class="d-flex justify-content-between eco-footer__bottom-wrap">
                <?php if (!empty($copyright_title)) : ?>
                    <?php echo wpautop( $copyright_title ); ?>
                <?php endif; ?>

                <div class="d-flex eco-footer__bottom-center">
                    <?php if (!empty($copyright_text)) : ?>
                        <?php echo wpautop( $copyright_text ); ?>
                    <?php endif; ?>

                    <?php wp_nav_menu(
                        array(
                            'container' => '',
                            'menu_class' => 'eco-footer__submenu d-none d-lg-flex',
                            'menu' => 'footer-menu-2',
                            'theme_location' => 'footer-menu',
                            'depth' => 1,
                        )
                    ); ?>
                </div>

                <a href="https://www.takeoffnyc.com/" class="eco-footer__link" target="_blank"><?php esc_html_e('Site by Takeoff', ECO_PREFIX); ?></a>
            </div>
        </div>
    </div>
</footer>

<?php get_template_part('template-parts/find-industry'); ?>
<?php get_template_part('template-parts/modal-search'); ?>

<?php wp_footer(); ?>
</body>
</html>
