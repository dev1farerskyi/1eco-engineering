<?php
/**
 * The file includes necessary functions for theme.
 *
 * @package default
 * @since 1.0
 */

if (!defined('ECO_PREFIX')) define('ECO_PREFIX', 'eco');
if (!defined('ECO_TEMP_PATH')) define('ECO_TEMP_PATH', get_template_directory());
if (!defined('ECO_TEMP_URL')) define('ECO_TEMP_URL', get_template_directory_uri());

// Register ACF Gravity Forms field
add_action( 'init', function () {
    if ( class_exists( 'ACF' ) ) {
        require_once get_stylesheet_directory() . '/inc/class-acf-field-gravity-v5.php';
    }
} );

require_once get_theme_file_path( '/inc/custom-post-type.php' );
require_once get_theme_file_path( '/inc/action-config.php' );
require_once get_theme_file_path( '/inc/helper-functions.php' );
require_once get_theme_file_path( '/inc/init-gutenberg.php' );
require_once get_theme_file_path( '/inc/menu-wrapper.php' );

function eco_after_theme_setup() {

    register_nav_menus(
        array(
            'main-menu' => esc_html__( 'Main menu', ECO_PREFIX ),
            'footer-menu' => esc_html__( 'Footer menu', ECO_PREFIX ),
            'footer-menu-2' => esc_html__( 'Footer menu 2', ECO_PREFIX ),
            'find-industry-menu' => esc_html__( 'Find industry menu', ECO_PREFIX ),
        )
    );

    add_theme_support( 'custom-header' );
    add_theme_support( 'automatic-feed-links' );
    add_theme_support( 'post-thumbnails' );
    add_theme_support( 'title-tag' );

    // Editor styles.
    add_theme_support('editor-styles');

    $editor_styles = array(
        'assets/css/style-editor.css'
    );
    add_editor_style($editor_styles); 
}

add_action( 'after_setup_theme', 'eco_after_theme_setup' );
