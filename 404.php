<?php
/**
 * 404 Page template.
 *
 * @package default
 * @since 1.0.0
 *
 */

get_header(); ?>

<div class="eco-hero">
    <div class="eco-hero__wrap">
        <div class="container">
            <div class="eco-hero__content">
                <h6 class="eco-hero__subtitle"><?php esc_html_e('404', 'eco'); ?></h6>
                <h1 class="eco-hero__title"><?php esc_html_e('Page Not found', 'eco'); ?></h1>
            </div>
        </div>
    </div>
</div>
<div class="eco-error-page">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-6 col-lg-8 col-sm-10">
                <div class="eco-error-page__wrap">
                    <div class="eco-error-page__image">
                        <img class="" src="<?php echo ( get_template_directory_uri() ); ?>/assets/img/404.svg" alt="address">
                    </div>
                    <h2 class="eco-block-title mb-20"><?php esc_html_e('Page Not found', 'eco'); ?></h2>
                    <p class="eco-block-subtitle mb-lg-50 mb-40"><?php esc_html_e('We’re sorry, the page you requested could not be found
                        Please go bake to the homepage', 'eco'); ?></p>
                    <a href="<?php echo home_url(); ?>" class="eco-btn eco-btn_icon eco-btn_main">
                        <?php esc_html_e('Go home', 'eco'); ?>
                        <span class="icon"></span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<?php get_footer(); ?>
