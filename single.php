<?php
/**
 * Single post template
 */

get_header();

$post_id = get_the_ID();

$author_id = get_post_field( 'post_author', $post_id );

$description = get_the_author_meta( 'description', $author_id );
$avatar = get_field( 'avatar', 'user_' . $author_id );
?>


<?php if ( have_posts() ) :
    while ( have_posts() ) : the_post(); ?>
        <div class="eco-hero eco-section-element">
            <div class="eco-hero__wrap">
                <div class="container">
                    <div class="eco-hero__content">
                        <h6 class="eco-hero__subtitle"><?php esc_html_e('Eco Engineering News', ECO_PREFIX); ?></h6>
                        <?php the_title('<h1 class="eco-hero__title">', '</h1>'); ?>
                        <?php get_template_part('template-parts/share-links'); ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="eco-article eco-section-element">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-xl-10">
                        <div class="eco-article__content">
                            <time><?php echo esc_html__('Published ', ECO_PREFIX) . get_the_date(); ?></time>
                            <?php the_post_thumbnail(); ?>
                            <?php the_content(); ?>
                        </div>

                        <?php get_template_part('template-parts/share-links'); ?>

                        <?php if ( ! empty( $avatar ) && ! empty( $description ) ): ?>
                            <div class="eco-article__author">
                                <div class="eco-article__author-photo">
                                    <img src="<?php echo esc_url($avatar['url']); ?>" alt="image">
                                </div>

                                <div class="eco-article__author-info">
                                    <p><?php echo $description; ?></p>
                                </div>

                                <div class="eco-article__author-bg"></div>
                            </div>
                        <?php endif ?>
                    </div>
                </div>
            </div>
        </div>

        <?php if ( get_field('call_to_action', 'option') ): ?>
            <?php $args = array(
                'classes' => 'eco-invite',
                'id' => 'eco-invite',
                'text' => get_field('cta_text', 'option'),
                'description' => get_field('cta_description', 'option'),
                'button' => get_field('cta_button', 'option'),
            );

            get_template_part( 'template-parts/elements/small-block', null, $args ); ?>
        <?php endif ?>

    <?php endwhile;
endif; ?>

<?php
/**
 * Recent posts
 */

$recent_args = array(
    'post_type' => 'post',
    'post_status' => 'publish',
    'posts_per_page' => -1,
    'post__not_in' => array(get_the_ID()),
    'category__in' => wp_get_post_categories(get_the_ID())
);

$recent_query = new WP_Query($recent_args);

if ( $recent_query->have_posts() ) : ?>
    <div class="eco-recent eco-section-element mb-125 mb-xl-170">
        <div class="container">
            <div class="eco-recent__head">
                <h2 class="eco-block-title mb-0"><?php esc_html_e('You might also be interested', ECO_PREFIX); ?></h2>

                <div class="swiper-cont">
                    <div class="swiper-button-prev swiper-recent-news-prev">
                        <span class="icon">
                            <svg width="7" height="12" viewBox="0 0 7 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M6 1L1 6L6 11" stroke="#0C2A50" stroke-linecap="round"></path>
                            </svg>
                        </span>
                    </div>

                    <div class="swiper-button-next swiper-recent-news-next">
                        <span class="icon">
                            <svg width="7" height="12" viewBox="0 0 7 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M1 11L6 6L1 1" stroke="#0C2A50" stroke-linecap="round"></path>
                            </svg>
                        </span>
                    </div>
                </div>
            </div>

            <div class="eco-recent__wrap">
                <div class="swiper swiper-recent-news">
                    <div class="swiper-wrapper">
                        <?php while ($recent_query->have_posts()) : $recent_query->the_post(); ?>
                            <div class="swiper-slide">
                                <?php get_template_part('template-parts/content', null, array('class' => 'news_short')); ?>
                            </div>
                        <?php endwhile; wp_reset_postdata(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>

<?php get_footer(); ?>
