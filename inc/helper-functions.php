<?php

/**
 * ACF Options page support
 */
if ( function_exists('acf_add_options_page') ) {
	acf_add_options_page();	
}

/**
 * Get link
 *
 * @param $link_arr
 * @param string $class
 */
function eco_btn($link_arr, string $class = '') {
	$class_btn = array();
	if ( ! empty( $class ) ) {
		$class_btn[] = $class;
	}
	if ( ! empty( $link_arr ) ) {
		$target = ! empty( $link_arr['target'] ) && $link_arr['target'] === '_blank' ? 'target="_blank"' : '';
		echo '<a class="' . esc_attr( implode(' ', $class_btn ) ) . '" href="' . esc_url( $link_arr['url'] ) . '" ' . $target . '>' . esc_attr( $link_arr['title'] ) . '<span class="icon"></span></a>';
	}
}
function eco_change_form_submit_button($button, $form)
{
	$class_name = 'cl-btn btn-green';
	$button_text = 'Get in touch';
	$button_element = '<svg class="button_element" width="29" height="29" viewBox="0 0 29 29" fill="none" xmlns="http://www.w3.org/2000/svg"> 
						<circle cx="14.5" cy="14.5" r="14" stroke="#0C2A50"/>
						</svg>';
	$button_element_2 = '<svg class="button_element_2" width="25" height="8" viewBox="0 0 25 8" fill="none" xmlns="http://www.w3.org/2000/svg">
						<path d="M24.3536 4.35355C24.5488 4.15829 24.5488 3.84171 24.3536 3.64645L21.1716 0.464466C20.9763 0.269204 20.6597 0.269204 20.4645 0.464466C20.2692 0.659728 20.2692 0.976311 20.4645 1.17157L23.2929 4L20.4645 6.82843C20.2692 7.02369 20.2692 7.34027 20.4645 7.53553C20.6597 7.7308 20.9763 7.7308 21.1716 7.53553L24.3536 4.35355ZM24 3.5L0 3.5V4.5L24 4.5V3.5Z" fill="#0C2A50"/>
						</svg>';
	return '<button type="submit" class="gform_button ' . $class_name . '" id="gform_submit_button_' . $form['id'] . '">' . $button_text . $button_element_2 . $button_element . '</button>';
}
add_filter('gform_submit_button', 'eco_change_form_submit_button', 10, 2);


/**
 * Change excerpt length
 *
 * @param $length
 * @return int
 */
function eco_change_excerpt_length( $length ) {
	return 30;
}
add_filter( 'excerpt_length', 'eco_change_excerpt_length', 999 );

/**
 * Change excerpt more
 *
 * @param $more
 * @return string
 */
function eco_change_excerpt_more( $more ) {
	return '...';
}
add_filter('excerpt_more', 'eco_change_excerpt_more');

/**
 * Sorting positions
 *
 * @return void
 */
function eco_sorting_positions() {
	$location = $_POST['location'];
	$department = $_POST['department'];
    $keyword = $_POST['s'];

	$args = array(
		'post_type' => 'open_positions',
		'post_status' => 'publish',
		'posts_per_page' => -1,
		'tax_query' => array(
			'relation' => 'AND',
		)
	);

    if ( ! empty( $keyword ) ) {
        $args['s'] = $keyword;
    }

	if ( $location !== 'all' ) {
		$args['tax_query'][] = array(
			'taxonomy' => 'location',
			'field'    => 'term_id',
			'terms'    => $location
		);
	}

	if ( $department !== 'all' ) {
		$args['tax_query'][] = array(
			'taxonomy' => 'department',
			'field'    => 'term_id',
			'terms'    => $department
		);
	}

	$positions_query = new WP_Query( $args );

	if ( $positions_query->have_posts() ) : ?>
		<?php while ( $positions_query->have_posts() ) : $positions_query->the_post();
			get_template_part('template-parts/position-content');
		endwhile; wp_reset_postdata();
	else :
		esc_html_e('Sorry, no positions matched your criteria.', ECO_PREFIX);
	endif;

	wp_die();
}
add_action('wp_ajax_sorting_positions', 'eco_sorting_positions');
add_action('wp_ajax_nopriv_sorting_positions', 'eco_sorting_positions');

/**
 * Sorting projects
 *
 * @return void
 */
function eco_sorting_projects() {
	$services = $_POST['services'];
	$industry = $_POST['industry'];
	$keyword = $_POST['s'];

	$args = array(
		'post_type' => 'projects',
		'post_status' => 'publish',
		'posts_per_page' => -1,
		'tax_query' => array(
			'relation' => 'AND',
		)
	);

	if ( ! empty( $keyword ) ) {
		$args['s'] = $keyword;
	}

	if ( $services !== 'all' ) {
		$args['tax_query'][] = array(
			'taxonomy' => 'projects_services',
			'field'    => 'term_id',
			'terms'    => $services
		);
	}

	if ( $industry !== 'all' ) {
		$args['tax_query'][] = array(
			'taxonomy' => 'projects_industry',
			'field'    => 'term_id',
			'terms'    => $industry
		);
	}

	$projects_query = new WP_Query( $args );

	if ( $projects_query->have_posts() ) : ?>
		<?php while ( $projects_query->have_posts() ) : $projects_query->the_post(); ?>
			<div class="col-lg-4 col-md-6">
				<?php get_template_part('template-parts/content'); ?>
			</div>
		<?php endwhile; wp_reset_postdata();
	else :
		esc_html_e('Sorry, no projects matched your criteria.', ECO_PREFIX);
	endif;

	wp_die();
}
add_action('wp_ajax_sorting_projects', 'eco_sorting_projects');
add_action('wp_ajax_nopriv_sorting_projects', 'eco_sorting_projects');



function eco_nav_menu_css_class($classes, $menu_item, $args, $depth) {
	if ( $args->theme_location == 'main-menu' && $depth == 0 ) {
		$style = get_field('menu_style', $menu_item);
		$classes[] = 'menu-item__' . $style;
	}

	return $classes;
}
add_filter('nav_menu_css_class', 'eco_nav_menu_css_class', 10, 4);


function eco_nav_menu_submenu_css_class($classes, $args, $depth) {
	if ( $args->theme_location == 'main-menu' ) {

		$classes[] = 'eco-header__submenu';

		if ( $depth == 0 ) {
			$classes[] = 'submenu_second';
		}
		
		if ( $depth == 1 ) {
			$classes[] = 'submenu_third';
		}

		if ( $depth == 2 ) {
			$classes[] = 'submenu_fourth';
		}
	}
	return $classes;
}
add_filter('nav_menu_submenu_css_class', 'eco_nav_menu_submenu_css_class', 10, 4);



function eco_nav_menu_item_args($args, $menu_item, $depth) {
	if ( $depth == 1 && $args->theme_location == 'main-menu' ) {
		$svg_icon = get_field('svg_icon', $menu_item);
		if ( ! empty( $svg_icon ) ) {
			$args->before = '<span class="submenu-icon">' . file_get_contents( $svg_icon ) . '</span>';
		}
	} else {
		$args->before = '';
	}

	return $args;
}
add_filter('nav_menu_item_args', 'eco_nav_menu_item_args', 10, 3);
// <span class="submenu-icon"></span>
// $args = apply_filters( 'nav_menu_item_args', $args, $menu_item, $depth );


