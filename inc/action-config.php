<?php


/**
 * Include google fonts.
 *
 * @return string
 */
function eco_fonts_url() {
    $fonts = array();

    $font_url = '';
    /*
    Translators: If there are characters in your language that are not supported
    by chosen font(s), translate this to 'off'. Do not translate into your own language.
     */
    if ( 'off' !== _x( 'on', 'Google font: on or off', ECO_PREFIX ) ) {
        $font_url = add_query_arg( 'family', ( implode( '|', $fonts ) . "&subset=latin,latin-ext" ), "//fonts.googleapis.com/css" );
    }
    return $font_url;
}

/**
 * Enqueue scripts.
 */
function eco_enqueue_scripts() {
    if ( is_admin() ) return false;

    wp_enqueue_style( 'inter-font', '//fonts.googleapis.com/css2?family=Inter:wght@400;500;600&display=swap' );
    wp_enqueue_style( ECO_PREFIX . '-select2', 'https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css' );
    wp_enqueue_style( 'magnific-popup', get_stylesheet_directory_uri() . '/assets/css/magnific-popup.css' );
    wp_enqueue_style( ECO_PREFIX . '-style', get_stylesheet_directory_uri() . '/assets/css/style.css' );

    wp_enqueue_script( ECO_PREFIX . '-select2', 'https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js', array('jquery'), '1.0.0', true );
    wp_enqueue_script( 'magnific-popup', get_stylesheet_directory_uri() . '/assets/js/jquery.magnific-popup.min.js', array('jquery'), '1.0.0', true );
    wp_enqueue_script( ECO_PREFIX . '-script', get_stylesheet_directory_uri() . '/assets/js/script.js', array('jquery'), '1.0.0', true );

    wp_localize_script( ECO_PREFIX . '-script', 'eco_object', array(
        'ajax_url' => admin_url( 'admin-ajax.php' ),
        'site_url' => site_url('/')
    ) );

}
add_action( 'wp_enqueue_scripts', 'eco_enqueue_scripts' );
