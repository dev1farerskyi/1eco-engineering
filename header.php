<?php
/**
 * Header template.
 *
 * @package default
 * @since 1.0.0
 *
 */



$contact_link = get_field('contact_link', 'options');

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="apple-mobile-web-app-capable" content="yes"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
    <meta name="format-detection" content="telephone=no"/>
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
    <header class="eco-header">
        <div class="container">
            <div class="d-flex eco-header__wrap">

                <a href="<?php echo get_home_url(); ?>" class="eco-header__logo logo_blue">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/logo.svg" alt="logo">
                </a>
                <a href="<?php echo get_home_url(); ?>" class="eco-header__logo logo_white">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/logo-white.svg" alt="logo">
                </a>

                <button class="eco-header__burger">
                    <span></span>
                    <span></span>
                    <span></span>
                </button>
                <div class="eco-header__nav">

                    <?php wp_nav_menu(
                        array(
                            'container' => '',
                            'items_wrap' => '<ul class="eco-header__menu">%3$s</ul>',
                            'theme_location' => 'main-menu',
                            'depth' => 4,
                            'walker' => new Submenu_wrap(),
                            'fallback_cb' => '__return_empty_string',
                        )
                    ); ?>

                    <button class="eco-btn eco-btn_search d-none d-lg-flex mr-20 js-modal" data-modal="modal-search">
                        <svg width="17" height="14" viewBox="0 0 17 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <circle cx="6.3" cy="6.3" r="5.6" stroke="white" stroke-width="1.4" />
                            <line x1="11.339" y1="9.70171" x2="15.848" y2="13.4627" stroke="white" stroke-width="1.4" />
                        </svg>
                    </button>

                    <?php if ( ! empty( $contact_link ) ) : ?>
                        <a href="<?php echo $contact_link; ?>" class="eco-btn eco-btn_icon eco-btn_main d-none d-lg-flex">
                            Contact us
                            <span class="icon"></span>
                        </a>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </header>

    <main class="main-wrapper">
