<form role="search" method="get" id="searchform"
      class="eco-searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>">
    <div class="eco-searchform__wrap">
        <label class="screen-reader-text" for="s"><?php _x( 'Search for:', 'label' ); ?></label>
        <input type="text" placeholder="Search" value="<?php echo get_search_query(); ?>" name="s" id="s" />
        <button class="eco-searchform__btn-mob" type="submit" id="searchsubmit">
            <?php echo file_get_contents(get_template_directory_uri() . '/assets/img/search_mob.svg'); ?>
        </button>
    </div>
</form>