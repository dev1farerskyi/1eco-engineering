import './app/gutenberg';
import Swiper from 'swiper/swiper-bundle';
import {gsap} from "./app/gsap/gsap";
import {ScrollTrigger} from "./app/gsap/ScrollTrigger";
import {isEven, isjQuery, Coordinates, videoResize} from "./app/functions";

gsap.registerPlugin(ScrollTrigger);


(function ($) {
    // Section Element
    let BCNoSectionElement = () => {
        let $wrapGlob = $('.main-wrapper > *')
        $wrapGlob.each(function () {
            let $self = $(this)

            if (!$self.hasClass('eco-section-element') && this.tagName !== 'STYLE') {
                $self.addClass('eco-section-default-element')
            }
        });
    }

    if (window.acf) {
        window.acf.addAction('render_block_preview', BCNoSectionElement)
    } else {
        BCNoSectionElement()
    }

    // Find industry
    $('.js-open-industry').on('click', function (){
        $(this).parent().addClass('active');
    });

    $('.js-close-industry').on('click', function (){
        $(this).closest('.eco-find-industry').toggleClass('active');
    });

    // results number counter animation

    const clResultsNum = document.querySelectorAll('.eco-statistics__col-number');

    function numberWithCommas(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }

    if (clResultsNum.length) {
        clResultsNum.forEach(item => {
            let target = {val: 0};
            const isNumFloat = item.innerText.includes('.');
            const tl = gsap.timeline(
                {
                    scrollTrigger: {
                        trigger: item,
                        start: 'top bottom',
                        scrub: false,
                    }
                }
            );


            tl.to(target, {
                val: item.innerHTML,
                duration: 2,
                onUpdate: function() {
                    isNumFloat ? item.innerText = target.val.toFixed(1) : item.innerText = numberWithCommas(Math.ceil(target.val));
                }
            }, 0);
        });
    }


    $('.d-header__burger').on('click', function (){
        $('.main_header').toggleClass('active');

    });

    $('input').focus(function(){
        $(this).parents('.form-group').addClass('focused');
    });

    $('input').blur(function(){
        var inputValue = $(this).val();
        if ( inputValue == "" ) {
            $(this).removeClass('filled');
            $(this).parents('.form-group').removeClass('focused');
        } else {
            $(this).addClass('filled');
        }
    })

    const tabs = document.querySelectorAll('.eco-tabs__btn');
    const tabContents = document.querySelectorAll('.tab-content-item');

    tabs.forEach(tab => {
        tab.addEventListener('click', function() {
            // remove active class from all tabs and tab content
            tabs.forEach(tab => tab.classList.remove('active'));
            tabContents.forEach(content => content.classList.remove('active'));

            // add active class to current tab and its corresponding tab content
            this.classList.add('active');
            document.getElementById(this.dataset.tabContent).classList.add('active');
        });
    });

    // Select2
    $('.select2').select2({
        minimumResultsForSearch: -1
    }).on('select2:select', function (e) {
        if ( e.target.classList.contains('js-change-map') ) {
            tabContents.forEach(content => content.classList.remove('active'));
            document.getElementById(e.target.value).classList.add('active');
        } else if ( e.target.classList.contains('js-change-awards') ) {
            document.querySelectorAll('.eco-info-tabs__box').forEach(content => content.classList.remove('active'));
            document.getElementById(e.target.value).classList.add('active');
        }
    });

    // Links share other
    $('.js-copy').on('click', function(e){
        var link = $(this);

        navigator.clipboard.writeText(link.data('copy'));
        link.addClass('active');

        e.preventDefault();
    });

    /*swiper*/
    const swiperReviews = new Swiper('.swiper-reviews', {
        speed: 400,
        slidesPerView: "auto",
        spaceBetween: 28,
        loop: false,
        breakpoints: {
            767: {
                slidesPerView: 2,
                spaceBetween: 38,
                loop: true,
            },
            1199: {
                slidesPerView: 3,
                spaceBetween: 48,
                loop: true,
            }
        },
        navigation: {
            nextEl: '.swiper-reviews-next',
            prevEl: '.swiper-reviews-prev',
        },
    });

    const swiperCustom = new Swiper('.swiper-custom', {
        speed: 600,
        slidesPerView: 1,
        spaceBetween: 28,
        loop: true,

        navigation: {
            nextEl: '.swiper-custom-next',
            prevEl: '.swiper-custom-prev',
        },
    });

    const swiperFeatured = new Swiper('.swiper-featured', {
        speed: 400,
        slidesPerView: 1,
        spaceBetween: 28,
        loop: false,
        breakpoints: {
            767: {
                slidesPerView: 1,
                spaceBetween: 38,
                loop: true,
            },
            1199: {
                slidesPerView: 1,
                spaceBetween: 48,
                loop: true,
            }
        },
        navigation: {
            nextEl: '.swiper-reviews-next',
            prevEl: '.swiper-reviews-prev',
        },
    });


    var galleryThumbs = new Swiper('.gallery-thumbs', {
        spaceBetween: 10,
        slidesPerView: "auto",
        freeMode: true,
        loop: true,
        watchSlidesProgress: true,
        breakpoints: {
            0: {
                slidesPerView: 1,
                spaceBetween: 48,
            },
            575: {
                slidesPerView: 2,

            },
            991: {
                slidesPerView: 3,

            },
        },
    });
    var galleryTop = new Swiper('.gallery-top', {
        spaceBetween: 10,
        slidesPerView: "auto",
        loop: false,
        navigation: {
            nextEl: '.swiper-custom-next',
            prevEl: '.swiper-custom-prev',
        },
        thumbs: {
            swiper: galleryThumbs
        },
        breakpoints: {
            0: {
                slidesPerView: 1,
            }
        },
    });


    const swiperSimple = new Swiper('.swiper-simple-slider', {
        speed: 400,
        slidesPerView: "auto",
        spaceBetween: 28,
        loop: false,
        breakpoints: {
            767: {
                slidesPerView: 2,
                spaceBetween: 48,
                loop: true,
            }
        },
    });

    const swiperPartners = new Swiper('.swiper-partners', {
        speed: 400,
        slidesPerView: 'auto',
        spaceBetween: 37,
        loop: false,
        breakpoints: {
            575: {
                spaceBetween: 40,
            },
            767: {
                spaceBetween: 50,
                slidesPerView: 4,
            },
            1199: {
                slidesPerView: 5,
            }
        },
    });

    const swiperRecentNews = new Swiper('.swiper-recent-news', {
        speed: 400,
        slidesPerView: "auto",
        spaceBetween: 20,
        loop: false,
        breakpoints: {
            767: {
                slidesPerView: 2,
                spaceBetween: 30,
            },
            992: {
                slidesPerView: 3,
                spaceBetween: 30,
            },
            1199: {
                slidesPerView: 3,
                spaceBetween: 58,
            }
        },
        navigation: {
            nextEl: '.swiper-recent-news-next',
            prevEl: '.swiper-recent-news-prev',
        },
    });

    const swipesImageCards = new Swiper('.swiper-image-card', {
        speed: 400,
        slidesPerView: 1,
        spaceBetween: 20,
        loop: true,
        navigation: {
            nextEl: '.swiper-image-card-next',
            prevEl: '.swiper-image-card-prev',
        },
    });

    const swiperTeam = new Swiper('.swiper-team', {
        speed: 400,
        slidesPerView: "auto",
        spaceBetween: 20,
        loop: false,
        breakpoints: {
            991: {
                spaceBetween: 40,
            },
            1199: {
                spaceBetween: 60,
            }
        },
        navigation: {
            nextEl: '.swiper-team-next',
            prevEl: '.swiper-team-prev',
        },
    });

    /*history slider*/
    var swiperHistoryNav = new Swiper(".swiper-history-nav", {
        loop: false,
        slidesPerView: 3,
        spaceBetween: 0,
        freeMode: false,
    });

    var swiperHistory = new Swiper(".swiper-history", {
        loop: true,
        spaceBetween: 10,
        navigation: {
            nextEl: ".swiper-history-next",
            prevEl: ".swiper-history-prev",
        },
        thumbs: {
            swiper: swiperHistoryNav,
        },
    });

    /*accordion*/
    $('.eco-accordion__header-btn').on('click', function () {
        let item = $(this).closest('.eco-accordion');
        if(!(item.hasClass("active"))) {
            $('.eco-accordion.active').removeClass('active');
            item.addClass('active');
        } else {
            item.removeClass('active');
        }
    });

    /*info tabs*/
    $('.eco-info-tabs__link').on('click', function () {
        let tab = $(this).attr('data-tab');
        if(!($(this).hasClass('active'))) {
            $('.eco-info-tabs__link.active').removeClass('active');
            $('.eco-info-tabs__box.active').removeClass('active');
            $(this).addClass('active');
            $('#' + tab).addClass('active');
        }
    });

    $('.js-change-info-tabs').on('change', function () {
        let _this = $(this),
            _val = _this.val();

        _this.closest('.eco-section_with_fixed_scroll').find('.eco-info-tabs__box.active').removeClass('active');
        _this.closest('.eco-section_with_fixed_scroll').find('#' + _val).addClass('active');
    });

    /*modal*/
    $('.js-modal').on('click', function (e) {
        e.preventDefault();

        let id = $(this).attr('data-modal');
        $('#' + id).addClass('show');

        $('body, html').css('overflow-y', 'hidden');
    });

    $('.eco-modal__close').on('click', function () {
        $(this).closest('.eco-modal').removeClass('show');
        $('body, html').css('overflow-y', 'visible');
    });


    /*header*/
    $('.eco-header__burger').on('click', function () {
        if($(this).hasClass('active')) {
            $('.eco-header').removeClass('show');
            $(this).removeClass('active');
            $('html').removeClass('modal-open');
        } else {
            $('.eco-header').addClass('show');
            $(this).addClass('active');
            $('html').addClass('modal-open');
        }
    })

    $('.has-child>a, .menu-item-has-children > a').on('click', function (e) {
        e.preventDefault();
        let li = $(this).parent();
        if($(li).hasClass('active')) {
            $(li).removeClass('active');
            $(this).next().removeClass('show')
        } else {
            $(li).addClass('active');
            $(this).next().addClass('show')
        }
    });

    $('.eco-header__menu').on('click', '.back-link', function () {
        $('.has-child.active').removeClass('active');
        $(this).closest('.show').removeClass('show');
    })

    // Share links
    $('[data-share]').on('click', function (e) {
        e.preventDefault();

        var w = window,
            url = $(this).attr('data-share'),
            title = '',
            w_pop = 600,
            h_pop = 600,
            scren_left = w.screenLeft != undefined ? w.screenLeft : screen.left,
            scren_top = w.screenTop != undefined ? w.screenTop : screen.top,
            width = w.innerWidth,
            height = w.innerHeight,
            left = ((width / 2) - (w_pop / 2)) + scren_left,
            top = ((height / 2) - (h_pop / 2)) + scren_top,
            newWindow = w.open(url, title, 'scrollbars=yes, width=' + w_pop + ', height=' + h_pop + ', top=' + top + ', left=' + left);

        if (w.focus) {
            newWindow.focus();
        }

        return false;
    });

    // Loader
    function eco_loader(status = 'show') {
        if ( status === 'remove' ) {
            setTimeout(function () {
                $('body, html').css('overflow-y', 'visible');
                $('body').find('.eco-loader').remove();
            }, 1000);
        } else {
            $('body, html').css('overflow-y', 'hidden');
            $('body').append('<div class="eco-loader"><svg viewBox="0 0 50 50"><circle class="path" cx="25" cy="25" r="20" fill="none" stroke-width="5"></circle></svg></div>');
        }
    }

    // Sorting positions
    $('.js-sorting-positions').on('submit', function (e) {
        e.preventDefault();

        let _this = $(this),
            _parent = _this.closest('.eco-open_positions'),
            _data = new FormData(_this[0]);

        $.ajax({
            url: eco_object.ajax_url + '?action=sorting_positions',
            processData: false,
            contentType: false,
            data: _data,
            type: 'POST',
            beforeSend: function( xhr ){
                eco_loader('add');
            },
            success: function (response) {
                let $items = $.parseHTML(response);

                _parent.find('.js-posts-wrapper').html($items);
                eco_loader('remove');
            }
        });

        return false;
    });

    // Sorting projects
    $('.js-sorting-projects').on('submit', function (e) {
        e.preventDefault();

        let _this = $(this),
            _parent = _this.closest('.eco-more_real_projects'),
            _data = new FormData(_this[0]);

        $.ajax({
            url: eco_object.ajax_url + '?action=sorting_projects',
            processData: false,
            contentType: false,
            data: _data,
            type: 'POST',
            beforeSend: function( xhr ){
                eco_loader('add');
            },
            success: function (response) {
                let $items = $.parseHTML(response);

                _parent.find('.js-posts-wrapper').html($items);
                eco_loader('remove');
            }
        });

        return false;
    });

    // menu
    $('.eco-header__menu > .menu-item-has-children').each(function(e) {
        var el = $(this),
            text = el.find('> a').text();

        el.find('.submenu_second').prepend('<li class="back-link">' + text + '</li>')
    });


    // video popup
    $('.js-magnific-video').magnificPopup({
        type: 'iframe'
    });

})(jQuery);
