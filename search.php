<?php
/**
 * Search template
 */

get_header();
?>

<div class="eco-hero">
    <div class="eco-hero__wrap">
        <div class="container">
            <div class="eco-hero__content">
                <h1 class="eco-hero__title">
                    <?php
                        printf(
                            esc_html__( 'Results for "%s"', ECO_PREFIX ),
                            '<span>' . esc_html( get_search_query() ) . '</span>'
                        );
                    ?>
                </h1>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <?php if ( have_posts() ) :
            while ( have_posts() ) : the_post(); ?>
                <div class="col-lg-4 col-md-6">
                    <?php get_template_part('template-parts/content'); ?>
                </div>
            <?php endwhile;
        endif; ?>
    </div>
</div>

<?php
get_footer();