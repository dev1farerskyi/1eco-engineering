<?php
get_header();

$repeater = get_field('repeater');
$button = get_field('button');

if ( have_posts() ) :
    while ( have_posts() ) : the_post(); ?>
        <div class="eco-hero eco-section-element">
            <div class="eco-hero__wrap">
                <div class="container">
                    <div class="eco-hero__content">
                        <h6 class="eco-hero__subtitle"><?php esc_html_e('Industry', ECO_PREFIX); ?></h6>
                        <?php the_title('<h1 class="eco-hero__title">', '</h1>'); ?>

                        <ul class="eco-social social_line mt-20 pt-5">
                            <?php eco_btn($button, 'eco-btn eco-btn_icon eco-btn_main mr-sm-30 mb-sm-0 mb-25'); ?>
                        </ul>
                    </div>
                </div>
            </div>

            <?php if ( ! empty( $repeater ) ) : ?>
                <div class="eco-statistics line_white position_left pos_top">
                    <div class="container">
                        <div class="eco-statistics__wrap">
                            <div class="eco-statistics__list">
                                <?php foreach( $repeater as $row ): ?>
                                    <?php $not_number = $row['not_number']; ?>
                                    <?php $number = $row['number']; ?>
                                    <?php $description = $row['description']; ?>
                                    <?php if($description): ?>
                                        <div class="eco-statistics__col">
                                            <div class="eco-statistics__col-number-count">
                                                <?php if($not_number): ?>
                                                    <h2 class="eco-statistics__col-number-after"><?php echo $not_number; ?></h2>
                                                <?php endif; ?>
                                                <?php if($number): ?>
                                                    <h2 class="eco-statistics__col-number">
                                                        <?php echo $number; ?>
                                                    </h2>
                                                <?php endif; ?>
                                            </div>
                                            <p class="eco-statistics__col-text"><?php echo $description; ?></p>
                                        </div>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
        </div>

        <?php the_content(); ?>

    <?php endwhile;
endif; ?>


<?php get_footer(); ?>
